#!/usr/bin/env python
import json
import asyncio
import websockets
import pprint
import fcntl
import termios
import struct

try:
    th, tw, hp, wp = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
except IOError:
    th, tw = 80, 200

pp = pprint.PrettyPrinter(indent=1, width=tw)


async def test(uri):
    async with websockets.connect(uri) as c:
        while True:
            pp.pprint(json.loads(await c.recv()))


while True:
    try:
        asyncio.get_event_loop().run_until_complete(test('ws://127.0.0.1:8785'))
    except Exception:
        pass
