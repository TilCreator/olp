import math

def mixRGB(rgb1, rgb2, fac):
    """
    Mixes two RGB values by a factor
    """
    inv = 1-fac
    return (rgb1[1]*fac+rgb2[1]*inv, 
            rgb1[2]*fac+rgb2[2]*inv,
            rgb1[3]*fac+rgb2[3]*inv)

def mixArray(rgbArray1, rgbArray2, fac):
    """
    Mixes two RGB Arrays by a factor or an array of factors
    """
    if type(fac) is list:
        for i in range (len(rgbArray1)):
            rgbArray1[i] = mixRGB(rgbArray1[i], rgbArray2[i], fac[i])
    else:
        for i in range (len(rgbArray1)):
            rgbArray1[i] = mixRGB(rgbArray1[i], rgbArray2[i], fac)
    return rgbArray1

def addRGB(rgb1, rgb2, fac=None):
    """
    Adds two RGB Values by a factor.
    Assumes factor 1 if no factor is given.
    """
    if (fac==None):
        return (rgb1[1]+rgb2[1], 
                rgb1[2]+rgb2[2],
                rgb1[3]+rgb2[3])
                
    return (rgb1[1]+rgb2[1]*fac, 
            rgb1[2]+rgb2[2]*fac,
            rgb1[3]+rgb2[3]*fac)

def addArray(rgbArray1, rgbArray2, fac):
    """
    Adds two RGB Arrays by a factor or an array of factors.
    Assumes factor 1 if no factor is given.
    """
    if type(fac) is list:
        for i in range (len(rgbArray1)):
            rgbArray1[i] = addRGB(rgbArray1[i], rgbArray2[i], fac[i])
    else:
        for i in range (len(rgbArray1)):
            rgbArray1[i] = addRGB(rgbArray1[i], rgbArray2[i], fac)
    return rgbArray1

def overrideRGB(rgb1, rgb2, flag = False):
    """
    Replaces RGB1 values with RGB2 where rgb2!=(0,0,0)
    Set flag true if you want to override a HSL value, where hsl2!=(*,*,0)
    """
    if (rgb2==(0,0,0) and not flag) or (rgb2[3]==0 and flag):
        return rgb2
    return rgb1

def overrideArray(rgbArray1, rgbArray2, flag=False):
    """
    Overrides RGB values of Array 1 with RGB values of Arrays2 where rgb2[i]!=(0,0,0)
    Set flag true if you want to override a HSL Array where hsl2[i]!=(*,*,0)
    """

    for i in range (len(rgbArray1)):
        rgbArray1[i] = mixRGB(rgbArray1[i], rgbArray2[i], flag)
    return rgbArray1

def normalizeRGB(rgb, flag = False):
    """
    Caps all values at 1, if flag isn't set, normalizes all values to 1, if flag is set.
    Caps al values below 0.
    """
    if flag:
        ref = 0
        for x in rgb:
            if x>ref: x = ref
        ref = 1/ref
        for x in rgb: x = x*ref
    else:
        for x in rgb:
            if x > 1 : x = 1
    
    if rgb[0] < 0: rgb[0] = 0
    if rgb[1] < 0: rgb[1] = 0
    if rgb[2] < 0: rgb[2] = 0
    
    return rgb

def normalizeRgbArray(rgb, flag = False):
    """
    Caps all values of array at 1, if flag isn't set, normalizes all values of array to 1, if flag is set.
    Be careful! This means if a single value is above 1, all other values will be dimmed down.
    Caps al values below 0.
    """
    if flag:
        ref = 0
        for x in rgb:
            for i in x:
                if i < 0: i = 0
                if i > ref: ref = i

        if ref > 1:
            ref = 1/ref
            for x in rgb:
                for i in x:
                    i = i*ref
    else:
        for i in rgb:
            i = normalizeRGB[i]
    return rgb

def normalizeHSV(hsv):
    """
    Applies modulo to hue, Caps saturation at 1, and caps value at 1.
    Caps saturation and value below 0.
    """
    hsv[0] = hsv[0]%1
    if hsv[1] > 1: hsv[1] = 1
    if hsv[2] > 1: hsv[2] = 1
    if hsv[1] < 0: hsv[1] = 0
    if hsv[2] < 0: hsv[2] = 0

    return hsv

def normalizeHsvArray(hsvArray):
    """
    Overrides RGB values of Array 1 with RGB values of Arrays2 where rgb2[i]!=(0,0,0)
    Set flag true if you want to override a HSL Array where hsl2[i]!=(*,*,0)
    Caps saturation and  value below 0.
    """

    for i in hsvArray:
        i = normalizeHSV(i)
    return hsvArray

def logEncode(rgb):
    return rgb*rgb

def logDecode(rgb):
    return math.sqrt(rgb)