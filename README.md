# TODO
- [ ] Implement multiple internal vars per Effect and selection per device (+ naming internal variables)
- [ ] Improve Effect Editor and Error handeling
- [ ] Implement Renaming into Webinterface
- [ ] Implemant Interface Events (If the server changes something)
- [ ] Sort and Check nodes (all using Color?)
- [ ] (Write actual tests)
- [ ] Finaly write fucking documentation

# Install
* Install python and coffeescript `# pacman -S python coffeescript`
* Download Repo `$ git clone https://gitlab.com/TilCreator/olp`
* Enter Repo `$ cd olp`
* Create virtual Python environment `$ python3 -m venv venv`
* Install needed packages into venv `$ venv/bin/pip install -r requirements.txt`
* Copy `config.example.json` to `config.json`
* Edit `config.json`
* Test start Server `$ venv/bin/python index.py`
* Access on [http://127.0.0.1:6410](http://127.0.0.1:6410)
* done

## Systemd Service
* Install in `/srv/olp`
* Create user `olp` `# useradd -d /srv/olp olp`
* Change owner of `/srv/olp` `# chown -R olp /srv/olp`
* Copy `olp.service` to `/etc/systemd/system`
* Reload daemons `# systemctl daemon-reload`
* Enable olp (start on boot) `# systemctl enable olp.service`
* Start olp (start on boot) `# systemctl start olp.service`

## Use external webserver (recommended)
* Set `webserver` in `config.json` to `false`
* Set `ws_port_server` and `ws_port_client` in `config.json`
* Setup your webserver of choise with the root `webui/build` and websocket proxy

# Nodes
- All nodes have access to config and animation

## Config
- in: dict of (name: type)
- out: dict of (name: type)
- name: str
- description: str
- type: str
- function: function
- class: class

# Drivers
- type: class
- nessesary class functions:
  - open
  - close
  - show

## Config
- args: dict of (name: type)
- name: str
- description: str

# Data types
- Float
- Number
- Bool
- Color
- Colors
- String

# WS API

## shortings
- config  
```python
    {'delay': number, 'driver': str, 'length': number}
```
- leds  
```python
    [[number, number, number], ...]
```
- animation  
```python
    {'name': str, 'active': bool, 'state': number, 'delta_time': number, 'speed': number, 'brightness': number, 'brightnessSpeed': number}
```
- device  
```python
    {'config': {<config>}, 'leds': [<leds>], 'animation': {<animation>}, 'error': {'short': '<short_error>', 'long': '<long_error>'} or None}
```
- effect_short  
```python
    {'description': '<description>'}
```
- effect  
```python
    {'description':'<description>', 'nodes': {<fbp_code>}}
```

## cmds
Return always includes cmd

Example:
```json
{"cmd": "change_device_name", "old_name": "Foo", "new_name": "Bar"}
```
=>
```json
{"devices": {"Bar": null}}
```

### devices
- get_devices   
```python
{}
=> {'devices': {'<name>': {<device>}}}
```
- get_device   
```python
{'name': '<name>'}
=> {'name': '<name>', 'data': {<device>}}
```
- set_device   
```python
{'name': '<name>', 'data': {<device>}}
=> {'name': '<name>', 'data': {<device>}}
```
- remove_device
```python
{'name': '<name>'}
=> {'devices': {'<name>': {<device>}}}
```
- add_device
```python
{'name': '<name>', 'data': '<data>'}
=> {'devices': {'<name>': {<device>}}}
```
- change_device_name
```python
{'old_name': '<old_name>', 'new_name': '<new_name>'}
=> {'devices': {'<name>': {<device>}}}
```

### effects
- get_effects   
```python
{}
=> {'effects': {'<name>': {<effect_short>}}}
```
- get_effect (returns only to one client)    
```python
{'name': '<name>'}
=> {'name': '<name>', 'data': {<effect>}}
```
- set_effect   
```python
{'name': '<name>', 'data': {<effect>}}
=> {'effects': {'<name>': {<effect_short>}}}
```
- remove_effect   
```python
{'name': '<name>'}
=> {'effects': {'<name>': {<effect_short>}}}
```
- add_effect   
```python
{'name': '<name>'}
=> {'effects': {'<name>': {<effect_short>}}}
```
- change_effect_name   
```python
{'old_name': '<old_name>', 'new_name': '<new_name>'}
=> {'effects': {'<name>': {<effect_short>}}}
```
- test_effect (returns only to one client)   
```python
{'data': {<effect>}}
=> {'pass': '<bool>', 'error': {'short': '<short_error>', 'long': '<long_error>', 'node_id': '<node_id>'}}
```

### globals
- get_globals
```python
{}
=> {'globals': {'name': ['<type>', '<value>'], ...}}
```
- set_global
```python
{'name': '<name>', 'data': '<value>'}
=> {'globals': {'name': ['<type>', '<value>'], ...}}
```
- add_global
```python
{'name': '<name>', 'data': '<value>', 'type': '<type>'}
=> {'globals': {'name': ['<type>', '<value>'], ...}}
```
- remove_global
```python
{'name': '<name>'}
=> {'globals': {'<name>': {'type': '<type>', 'value': '<value>'}, ...}}
```
- change_global_name
```python
{'old_name': '<old_name>', 'new_name': '<new_name'}
=> {'globals': {'<name>': {'type': '<type>', 'value': '<value>'}, ...}}
```

### other
- get_data_types
```python
{}
=> {'data_types': {'<name>': '<html>', ...}}
```
- get_nodes
```python
{}
=> {'nodes': {'<name>': {<node>}, ...}}
```
- get_drivers
```python
{}
=> {'drivers': {'<name>': {'name': '<name>', 'description': '<description>', 'args': {'<name>': '<type>', ...}}, ...}}
```
