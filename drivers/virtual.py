import pygame


class Driver:
    def __init__(self, config):
        pygame.init()

        self.size = (config['length'] * 10, 10)

        self.screen = pygame.display.set_mode(self.size, pygame.RESIZABLE)
        self.s = pygame.Surface(pygame.display.get_surface().get_size(), pygame.SRCALPHA)

        pygame.display.set_caption(config['name'])

    def open(self):
        pass

    def close(self):
        pass

    def show(self, leds, animation):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.VIDEORESIZE:
                self.size = event.dict['size']

                self.screen = pygame.display.set_mode(self.size, pygame.RESIZABLE)
                self.s = pygame.Surface(pygame.display.get_surface().get_size(), pygame.SRCALPHA)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_F11:
                pygame.display.toggle_fullscreen()

                self.screen = pygame.display.set_mode(self.size, pygame.RESIZABLE)

                self.s = pygame.Surface(pygame.display.get_surface().get_size(), pygame.SRCALPHA)

        width, height = pygame.display.get_surface().get_size()

        self.screen.fill((0, 0, 0))
        self.s.fill((0, 0, 0))

        r_w = width / len(leds)
        for i, led in enumerate(leds):
            pygame.draw.rect(self.s, pygame.Color(*[int(max(min(color, 1), 0) * 255) for color in led.getRgb()], 255), [i * r_w, -height, r_w, height * 2])

        self.screen.blit(self.s, (0, 0))

        pygame.display.flip()


attributes = {
    'name': 'Virtual',
    'description': 'Virtual test "LED" strip. Spawns as an Window on the Server. (Warning: Broken color)',
    'args': {},
    'class': Driver
}
