import socket


class Driver:
    """Class to throw UDP packets onto an ESP with a connected LED strip."""

    def __init__(self, config):
        self.config = config
        self.target = (config['host'], config['port'])

        if self.config['extra white led']:
            self.leds = [(0, 0, 0, 0) for _ in range(config['length'])]
        else:
            self.leds = [(0, 0, 0) for _ in range(config['length'])]
        self.lastLeds = []

        self.mapping = [self.config['mapping'].lower().find(char) for char in 'rgbw']

        for n in self.mapping:
            if n < 0 or (n > 2 and not self.config['extra white led']) or (n > 3 and self.config['extra white led']):
                raise ValueError('Mapping error: Make sure to only use "rgb" or "rgbw" (or in differend order)')

    def open(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def close(self):
        self.sock.close()

    def show(self, leds, animation):
        if len(leds) == len(self.leds):
            if self.config['extra white led']:
                self.leds = [[led.getRgbw()[self.mapping[i]] for i in range(4)] for led in leds]
            else:
                self.leds = [[led.getRgb()[self.mapping[i]] for i in range(3)] for led in leds]
        else:
            IndexError('Lists don\'t have the same length')

        if self.leds != self.lastLeds:
            self._send(self._encode())

            self.lastLeds = self.leds.copy()

            return True
        return False

    def _encode(self):
        return bytes([int(max(min(color, 1), 0) * 255) for led in self.leds for color in led])

    def _send(self, data):
        if self.config['use new protocol']:
            data = bytes([int(len(data) / 256), len(data) % 256]) + data

        data_send = 0
        while len(data) - data_send > 0:
            data_send += self.sock.sendto(data[data_send:], self.target)


attributes = {
    'name': 'ESP with WS2812 or similar',
    'description': 'Driver for WS2812 or similar LED stripes controlled by an ESP8622 connected over WIFI',
    'args': {'host': 'String', 'port': 'Number', 'extra white led': 'Bool', 'use new protocol': 'Bool', 'mapping': 'String'},
    'class': Driver
}
