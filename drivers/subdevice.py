from color import Color


class Driver:
    """Class to divide strips into multiple pices."""

    def __init__(self, config, devices):
        self.config = config
        self.devices = devices
        self.KeyError_count = 0

    def open(self):
        if self.config['root'] not in self.devices.keys():
            self.KeyError_count += 1

        # if not self.devices[self.config['root']].animation['active']:
        #     raise NameError(f'You have to enable "{self.config["root"]}" first')

    def close(self):
        pass

    def show(self, leds, animation):
        # if not self.devices[self.config['root']].animation['active']:
        #     raise NameError(f'You have to enable "{self.config["root"]}" first')

        try:
            self.devices[self.config['root']].effect = None
            self.devices[self.config['root']].animation['name'] = None

            if self.config['use_brightness_of_root']:
                [led.dim(self.devices[self.config['root']].animation['brightness']) for led in leds]

            if self.devices[self.config['root']].animation['active']:
                self.devices[self.config['root']].leds[self.config['start']:self.config['start'] + self.config['length']] = leds
        except KeyError:
            self.KeyError_count += 1

            if self.KeyError_count > 20:
                raise KeyError(f'Root device "{self.config["root"]}" does not exist')


attributes = {
    'name': 'Subdevice',
    'description': 'Driver to divide strips into multiple pices',
    'args': {'root': 'String', 'start': 'Number', 'use_brightness_of_root': 'Bool'},
    'init_with_devices': True,
    'class': Driver
}
