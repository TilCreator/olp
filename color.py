import colorsys
import copy
from colorsys import rgb_to_hsv

def add(org, col, fac=1, gammaCorrect=True): return copy.deepcopy(org).add(fac)

def overlay(org, col, fac): return copy.deepcopy(org).overlay(fac)

def mixCol(org, col, fac): return copy.deepcopy(org).mixCol(fac)

def mixHue(org, col, fac): return copy.deepcopy(org).mixHue(fac)

def mixSaturation(org, col, fac): return copy.deepcopy(org).mixSaturation(col, fac)

def mixValue(org, col, fac, gammaCorrect=True): return copy.deepcopy(org).mixValue(col, fac, gammaCorrect)

def mix(org, col, fac=1, gammaCorrect=True): return copy.deepcopy(org).mix(col, fac, gammaCorrect)

def multiply(org, col, fac=1, gammaCorrect=True): return copy.deepcopy(org).multiply(col, fac, gammaCorrect)

def dim(org, fac = 0.5):return copy.deepcopy(org).dim(fac)

def override(org, col):return copy.deepcopy(org).override(col)

def _capHSV(hsv):
    """
    Applies modulo to hue, Caps saturation at 1, and caps value at 1.
    Caps saturation and value below 0.
    """
    hsv[0] = hsv[0] % 1
    if hsv[1] > 1: hsv[1] = 1
    if hsv[2] > 1: hsv[2] = 1
    if hsv[1] < 0: hsv[1] = 0
    if hsv[2] < 0: hsv[2] = 0

    return hsv


def _hsvToRgb(hsv, linear = True):
    """
    Converts a given HSV-List to an RGB List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    if linear:
        enc = colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2]**2.2)
        return [enc[0]**0.45,
                enc[1]**0.45,
                enc[2]**0.45]

    enc = colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2])
    return [enc[0],
            enc[1],
            enc[2]]


def _hsvToRgbw(hsv, linear=True):
    """
    Converts a given HSV-List to an RGBW List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    rgbw = _hsvToRgb([hsv[0], 1, hsv[1]*hsv[2]], linear)
    rgbw.append((1-hsv[1])*hsv[2])
    return rgbw


def _capRGB(rgb, flag=False):
    """
    Caps all values at 1, if flag isn't set, normalizes all values to 1, if flag is set.
    Caps all values below 0.
    """
    if flag:
        ref = 0
        for x in rgb:
            if x > ref: x = ref
        ref = 1/ref
        for x in rgb: x = x*ref
    else:
        for x in rgb:
            if x > 1: x = 1

    if rgb[0] < 0: rgb[0] = 0
    if rgb[1] < 0: rgb[1] = 0
    if rgb[2] < 0: rgb[2] = 0

    return rgb


def _rgbToHsv(rgb, linear=True):
    """
    Converts a given RGB-List to an HSV List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """
    if linear:
        enc = colorsys.rgb_to_hsv(rgb[0]**2.2, rgb[1]**2.2, rgb[2]**2.2)
        return [enc[0], enc[1], enc[2]**0.45]

    enc = colorsys.rgb_to_hsv(rgb[0], rgb[1], rgb[2])
    return [enc[0], enc[1], enc[2]]


def _rgbToRgbw(rgb, linear=True):
    """
    Converts a given RGB-List to an RGBW List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """
    return _hsvToRgbw(rgb_to_hsv(*rgb), linear)


def _rgbToHex(rgb):
    """
    Converts a given RGB-Array to an HEX-Code (Type: String).
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """
    return f'{int(rgb[0] * 255):02x}{int(rgb[1] * 255):02x}{int(rgb[2] * 255):02x}'

def _rgbwToHex(rgbw):
    """
    Converts a given RGBW-Array to an HEX-Code (Type: String).
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """
    rgb = _rgbwToRgb(rgbw)
    return f'{int(rgb[0] * 255):02x}{int(rgb[1] * 255):02x}{int(rgb[2] * 255):02x}'

def _hsvToHex(hsv):
    """
    Converts a given HEX-Array to an HEX-Code (Type: String).
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """
    rgb = _hsvToRgb(hsv)
    return f'{int(rgb[0] * 255):02x}{int(rgb[1] * 255):02x}{int(rgb[2] * 255):02x}'

def _hexToRgb(hexCode):
    """
    Converts a given HEX-Code to an RGB-List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    if hexCode[:1] == '#':
        hexCode = hexCode.lstrip('#')
    return [int(hexCode[i:i+2], 16) / 255 for i in (0, 2, 4)]

def _hexToRgbw(hexCode):
    """
    Converts a given HEX-Code to an RGBW-List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    return _rgbToRgbw(_hexToRgb(hexCode))

def _hexTohsv(hexCode):
    """
    Converts a given HEX-Code to an HSV-List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    return _rgbToHsv(_hexToRgb(hexCode))

def _capRgbw(rgbw, flag=False):
    """
    Caps all values at 1, if flag isn't set, normalizes all values to 1, if flag is set.
    Caps al values below 0.
    """
    if flag:
        ref = 0
        for x in rgbw:
            if x > ref: x = ref
        ref = 1/ref
        for x in rgbw: x = x*ref
    else:
        for x in rgbw:
            if x > 1: x = 1

    if rgbw[0] < 0: rgbw[0] = 0
    if rgbw[1] < 0: rgbw[1] = 0
    if rgbw[2] < 0: rgbw[2] = 0
    if rgbw[3] < 0: rgbw[3] = 0

    return rgbw


def _rgbwToHsv(rgbw):
    """
    Converts a given RGBW-List to an HSV-List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    return _rgbToHsv(_rgbwToRgb(rgbw))


def _rgbwToRgb(rgbw):
    """
    Converts a given RGBW-List to an RGB-List.
    Expects a List or an Array of 3 Floats with a Range of 0-1.
    Assues linear Color Space, if 'linear' isn't set False.
    """

    return [rgbw[0]+rgbw[3], rgbw[1]+rgbw[3], rgbw[2]+rgbw[3]]


class Color():
    """
    Initiates a Color Object based on a List of 3-4 Components, a Mode, a Gamma-value and informatoin, whether the components are encoded or not.
    Defaults all values of the Component to [0,0,0,0]. Expects floats in the range of 0 to 1.
    Supports both Strings and Integers as Mode input. RGB:1, RGBW:2, HSV:3, HEX:4
    Defaults Gamma at 2.2, which worked best for our LEDs.
    Assumes Components to be not gamma encoded, set encoded to True to have components converted to the internal linear color space by constructor.
    """
    def __init__(self, components=[0, 0, 0, 0], mode=1, gamma=2.2, encoded=False):
        self.gamma = gamma
        self.max = 1

        if isinstance(mode, str):
            if mode.lower() == 'hex': mode = 4
            elif mode.lower() == 'hsv': mode = 3
            elif mode.lower() == 'rgbw': mode = 2
            elif mode.lower() == 'rgb': mode = 1

        if mode == 4:
            self.mode = 1
            self._cmp = components = _hexToRgb(components)
            if encoded:
                for i in components:
                    i = i ^ 1/gamma

        elif mode == 3:
            self.mode = 3
            if encoded:
                self.setHsvEncoded(components)
            else:
                self._cmp = [components[0] % 1,
                             components[1],
                             components[2]]
            return

        elif mode == 2:
            self.mode = 2
            if encoded:
                self.setRgbwEncoded(components)
            else:
                self._cmp = [components[0],
                             components[1],
                             components[2],
                             components[3],
                             ]
            return

        elif mode == 1:
            self.mode = 1
            if encoded:
                self.setRgbEncoded(components)
            else:
                self._cmp = [components[0],
                             components[1],
                             components[2],
                             ]
        else:
            raise KeyError('Unknown mode')
            #print (self._rgb + self._hsv + self._rgbw)

    def __repr__(self):
        return f"""Color(components={self._cmp}, mode='{self.mode}', gamma={self.gamma})"""

    def _convert(self, mode, dest=False):
        if dest:
            if mode == self.mode:
                self._tmp = self._cmp
                return
            if mode == 1:
                if self.mode == 2:
                    self.normalize(2)
                    self._cmp = _rgbwToRgb(self._cmp)
                if self.mode == 3:
                    self.normalize(3)
                    self._cmp = _hsvToRgb(self._cmp)
                self.deNormalize()
            if mode == 2:
                self.normalize(2)
                if self.mode == 1:
                    self.normalize(1)
                    self._cmp = _rgbToRgbw(self._cmp)
                if self.mode == 3:
                    self.normalize(3)
                    self._cmp = _hsvToRgbw(self._cmp)
                self.deNormalize()
            if mode == 3:
                self.normalize(3)
                if self.mode == 1:
                    self.normalize(1)
                    self._cmp = _rgbToHsv(self._cmp)
                if self.mode == 2:
                    self.normalize(2)
                    self._cmp = _rgbwToHsv(self._cmp)
                self.deNormalize()
            self._tmp = self._cmp
        else:
            if mode == self.mode:
                self._tmp = self._cmp
            if mode == 1:
                if self.mode == 2:
                    self.normalize(2)
                    self._tmp = _rgbwToRgb(self._cmp)
                if self.mode == 3:
                    self.normalize(3)
                    self._tmp = _hsvToRgb(self._cmp)
                self.deNormalize(1)
            if mode == 2:
                self.normalize(2)
                if self.mode == 1:
                    self.normalize(1)
                    self._tmp = _rgbToRgbw(self._cmp)
                if self.mode == 3:
                    self.normalize(3)
                    self._tmp = _hsvToRgbw(self._cmp)
                self.deNormalize(2)
            if mode == 3:
                self.normalize(3)
                if self.mode == 1:
                    self.normalize(1)
                    self._tmp = _rgbToHsv(self._cmp)
                if self.mode == 2:
                    self.normalize(2)
                    self._tmp = _rgbwToHsv(self._cmp)
                self.deNormalize(3)

    def getRgbEncoded(self, convert=False):
        """
        Returns Gamma-encoded RGB Value of color. Does not change it's mode to RGB unless flag is set to True. Use this to send colors to a RGB-LED.
        """
        self._convert(1, convert)
        return [self._tmp[0] ** (self.gamma),
                self._tmp[1] ** (self.gamma),
                self._tmp[2] ** (self.gamma)]

    def setRgbEncoded(self, rgb, gamma=None):
        """
        Sets Encoded RGB-Value, changes Mode to RGB.
        """
        if gamma is None: gamma = self.gamma
        self.setRgb([rgb[0] ** (1 / self.gamma),
                     rgb[1] ** (1 / self.gamma),
                     rgb[2] ** (1 / self.gamma)])
        self.mode = 1

    def getRgbwEncoded(self, convert=False):
        """
        Returns Gamma-encoded RGBW Value of color. Does not change it's mode to RGBW unless flag is set to True. Use this to send colors to a RGBW-LED.
        """
        self._convert(2, convert)
        return [self._tmp[0] ** (self.gamma),
                self._tmp[1] ** (self.gamma),
                self._tmp[2] ** (self.gamma),
                self._tmp[3] ** (self.gamma)]

    def setRgbwEncoded(self, rgbw, gamma=None):
        """
        Sets Encoded RGBW-Value, changes Mode to RGBW.
        """
        if gamma is None: gamma = self.gamma
        self._cmp = [rgbw[0] ** (1 / self.gamma),
                     rgbw[1] ** (1 / self.gamma),
                     rgbw[2] ** (1 / self.gamma),
                     rgbw[3] ** (1 / self.gamma)]
        self.mode = 2

    def getHsvEncoded(self, convert=False):
        """
        Returns Gamma-encoded HSV Value of color. Does not change it's mode to HSV unless flag is set to True.
        """
        self._convert(3, convert)
        return [self._tmp[0],
                self._tmp[1],
                self._tmp[2] ** (self.gamma)]

    def setHsvEncoded(self, hsv, gamma=None):
        """
        Sets Encoded HSV-Value, changes Mode to HSV.
        """
        if gamma is None: gamma = self.gamma
        self.mode = 3
        self._cmp[0] = hsv[0] % 1
        self._cmp[1] = hsv[1]
        self._cmp[2] = hsv ** (1 / self.gamma)

    def setRgb(self, rgb):
        """
        Sets linear RGB Value. Changes Mode to RGB.
        """
        self.mode = 1
        self._cmp = rgb

    def setHsv(self, hsv):
        """
        Sets linear HSV Value. Changes Mode to HSV.
        """
        self.mode = 2
        self._cmp = hsv

    def setRgbw(self, rgbw):
        """
        Sets linear RGBW Value. Changes Mode to RGBW.
        """
        self.mode = 3
        self._cmp = rgbw

    def getRgbw(self, convert=False):
        """
        Returns linear RGBW Value of color. Does not change it's mode to RGBW unless flag is set to True.
        """
        self._convert(2, convert)
        return self._tmp

    def getRgb(self, convert=False):
        """
        Returns linear RGB Value of color. Does not change it's mode to RGB unless flag is set to True.
        """
        self._convert(1, convert)
        return self._tmp

    def getHsv(self, convert=False):
        """
        Returns linear HSV Value of color. Does not change it's mode to HSV unless flag is set to True.
        """
        self._convert(3, convert)
        return self._tmp

    def getHex(self):
        """
        Returns linear HEX Value of color. Does not change it's mode to HEX or RGB.
        """
        self._convert(1, False)
        return _rgbToHex(self._tmp)

    def getHexEncoded(self):
        """
        Returns Gamma-encoded HEX Value of color. Does not change it's mode to HEX or RGB.
        """
        self._convert(1, False)
        return _rgbToHex([self._tmp[0]**self.gamma,
                          self._tmp[1]**self.gamma,
                          self._tmp[2]**self.gamma])

    def normalize(self, mode=0):
        """
        Normalizes all Values of Component to fit in range between 0 and 1. Remembers highest value, if a value was above 1 so they may be denormalized.
        """
        ref = 1
        if not (mode == 3):
            for x in self._cmp:
                if x > ref: x = ref
        else:
            self._cmp[0] = self._cmp[0] % 1
            if self._cmp[1] > 1: self._cmp[1] = 1
            if self._cmp[1] < 0: self._cmp[1] = 0
            if self._cmp[2] > ref: ref = self._cmp[2]

        if ref == 1: return

        self.max = ref
        ref = 1/ref
        if mode == 1 or mode == 2:
            for x in self._cmp: x = x*ref
        else:
            self._cmp[2] = self._cmp[2]*ref
            _capHSV(self._cmp)

            #print ("meep")
    def deNormalize(self, tmpMode=None):
        """
        Reverts effects of normaize funciton.
        """
        if not self.max == 1:
            if self.mode == 1 or self.mode == 2:
                for x in self._cmp: x = x*self.max
            if self.mode == 3:
                self._cmp[2] = self._cmp[2] * self.max

            #print ("meep")
        if tmpMode is None: return
        if not tmpMode == 1:
            if self.mode == 1 or self.mode == 2:
                for x in self._tmp: x = x*self.max
            if self.mode == 3:
                self._tmp[2] = self._tmp[2] * self.max

    def add(self, col, fac=1, gammaCorrect=True):
        """
        Adds a second color to this color object by a factor. Will by default use linear color space so results are physically accurate.
        By setting gammaCorrect to False, results of traditional 2D-Applications will be replicated. This however comes at a slight performance disatvantage. It is reccomended to use the default linear color space.
        Will keep the mode if possible, but HSV will be cast to RGB, since HSV color space does not support this operation.
        """
        if not gammaCorrect:
            if self.mode == 2:
                add = col.getRgbwEncoded()
                self._cmp = self.getRgbwEncoded()
                self.setRgbwEncoded([self._cmp[0]+(fac*add[0]),
                                     self._cmp[1]+(fac*add[1]),
                                     self._cmp[2]+(fac*add[2]),
                                     self._cmp[3]+(fac*add[3])])

            else:
                self._convert(1, True)
                add = col.getRgbEncoded()
                self._rgb = self.getRgbEncoded()
                self.setRgbEncoded([self._rgb[0]+(fac*add[0]),
                                    self._rgb[1]+(fac*add[1]),
                                    self._rgb[2]+(fac*add[2])])

        else:
            if self.mode == 2:
                self.setRgbw([self._cmp[0]+(col._cmp[0]*fac),
                              self._cmp[1]+(col._cmp[1]*fac),
                              self._cmp[2]+(col._cmp[2]*fac),
                              self._cmp[3]+(col._cmp[3]*fac)])
            else:
                self._convert(1, True)
                self.setRgb([self._cmp[0]+(col._cmp[0]*fac),
                             self._cmp[1]+(col._cmp[1]*fac),
                             self._cmp[2]+(col._cmp[2]*fac)])

        return self

    def overlay(self, col, fac):
        """
        Adds or substracts the difference between a colors components and 0.5 to this colors values.
        """

        if self.mode == 1:
            ov = col.getRgb()
            col.mode = 1
        if self.mode == 2:
            ov = col.getRgbw()
            col.mode = 2
        if self.mode == 3:
            ov = col.getHsv()
            col.mode = 3

        for i in range(len(self._cmp)):
            self._cmp[i] = self._cmp[i] + ((ov[i]-0.5)*fac)

        return self

    def mixCol(self, col, fac):
        """
        Mixes a second Colors Saturation and Hue into this Color by a factor.
        This will change it's own mode to HSV.
        """
        self._convert(3, True)
        self._cmp[0] = self._cmp[0]*(1-fac)+col.getHsv()[0]*fac
        self._cmp[1] = self._cmp[1]*(1-fac)+col.getHsv()[1]*fac

        return self

    def mixHue(self, col, fac):
        """
        Mixes a second Colors Hue into this Color by a factor.
        This will change it's own mode to HSV.
        """
        self._convert(3, True)
        self._cmp[0] = self._cmp[0]*(1-fac)+col.getHsv()[0]*fac

        return self

    def mixSaturation(self, col, fac):
        """
        Mixes a second Colors Saturation into this Color by a factor.
        This will change it's own mode to HSV.
        """
        self._convert(3, True)
        self._cmp[1] = self._cmp[1]*(1-fac)+col.getHsv()[1]*fac

        return self

    def mixValue(self, col, fac, gammaCorrect=True):
        """
        Mixes a second Colors Valuee into this Color by a factor. Will use a linear Color Space by default.
        To reproduce results of some traditional 2D-applications, you may set gammaCorrect to False. This however comes at a performance disatvantage.
        This will change it's own mode to HSV.
        """
        self._convert(3, True)
        if gammaCorrect():
            self._cmp[2] = self._cmp[2]*(1-fac)+col.getHsv()[2]*fac
        else:
            self._cmp[2] = self.getHsvEncoded()[2]*(1-fac)+col.getHsvEncoded()[2]*fac
            self.gamma = self.gamma*(1-fac) + col.gamma*fac

        return self

    def mix(self, col, fac=1, gammaCorrect=True):
        """
        Blends a second Colors into this Color by a factor. Will use a linear Color Space by default.
        To reproduce results of some traditional 2D-applications, you may set gammaCorrect to False. This however comes at a performance disatvantage.

        Note: by blending a color in HSV-Mode, hue will shift around the color wheel as you change the factor without changing saturation.
        To get a desaturated intermediate when blending two colors of identical saturation and big hue difference, convert Color to RGB or RGBW first.
        """
        if self.mode == 1 and gammaCorrect: mix = col.getRgb()
        if self.mode == 2 and gammaCorrect: mix = col.getRgbw()
        if self.mode == 3 and gammaCorrect: mix = col.getHsv()
        if self.mode == 1 and not gammaCorrect:
            mix = col.getRgbEncoded()
            org = self.getRgbEncoded()
        if self.mode == 2 and not gammaCorrect:
            mix = col.getRgbwEncoded()
            org = self.getRgbwEncoded()
        if self.mode == 3 and not gammaCorrect:
            mix = col.getHsvEncoded()
            org = self.getHsvEncoded()
        if gammaCorrect:
            for i in range(len(self._cmp)):
                self._cmp[i] = self._cmp[i] * (1-fac) + mix[i]*fac
        else:
            for i in range(len(self._cmp)):
                self._cmp[i] = org[i] * (1-fac) + mix[i]
                self.gamma = self.gamma*(1-fac) + col.gamma*fac

        return self

    def multiply(self, col, fac=1, gammaCorrect=True):
        if gammaCorrect:
            if self.mode == 3:
                self._convert(1, True)
            if self.mode == 1:
                mul = col.getRgb()
            else:
                mul = col.getRgbw()
            self._cmp = [self._cmp[i] * m * fac + self._cmp[i] * 1 - fac for i, m in enumerate(mul)]

        return self

    def dim(self, fac = 0.5):
        """
        Dims the array by a factor. This factor is set to 0.5 by default.
        If the value is at one, the light is above 1, it get's brighter, if the value is below 1, the light gets darker.
        """
        if self.mode == 3:
            self._cmp[2] *= fac
        else:
            self._cmp = [val*fac for val in self._cmp]

        return self

#   def setValue(self, col, fac=1, gammaCorrect=True):
#      self._convert(3, True)
#       if fac == 1:
#           self._cmp[2] = col.getHsv()[2]
#           if not self.gamma == col.gamma and not gammaCorrect: self.gamma = col.gamma()
#       else:
#           if not self.gamma == col.gamma and not gammaCorrect:
#               self.setHsvEncoded([self._cmp[0],
#                                   self._cmp[1],
#                                   self._cmp[2]**self.gamma*(1-fac)+col.getHsvEncoded()[2]*fac])
#           else:
#               self.setHsv([self._cmp[0],
#                            self._cmp[1],
#                            self._cmp[2]*(1-fac)+col.getHsv()[2]*fac])

    def override(self, col):
        """
        Will replace Color  with Copy of second Color, if second Color is not black.
        """
        if col.mode == 1:
            if (col._cmp[0] > 0 or col._cmp[1] > 0 or col._cmp[2] > 0): flag = True
        if col.mode == 2:
            if (col._cmp[0] > 0 or col._cmp[1] > 0 or col._cmp[2] > 0 or col._cmp[3] > 0): flag = True
        if col.mode == 3:
            if (col._cmp[2] > 0): flag = True
        if flag:
            self.mode = col.mode
            self.gamma = col.gamma
            self.max = col.max
            self._cmp = copy.deepcopy(col._cmp)
        
        return self


def test():
    #print(hexToRgb('#123456'))

    #col1 = Color([0, 0.5, 0.5, 0.5], mode='rgbw', gamma=2.2, encoded=False)
    #print("HSV: " + col1.getHsv().__str__())
    #print("RGB: " + col1.getRgb().__str__())
    #print("RGBW: " + col1.getRgbw().__str__())
    #print(col1._cmp)
    
    col1 = Color([0.5, 0.5, 0.5])
    col2 = Color([0.5, 0.5, 0.5], 'hsv')
    col3 = Color([0.7, 0.7, 0.7, 1], 'rgbw')
    
    col1.dim()
    col2.dim()
    col3.dim()
    
    print(col1)
    print(col2)
    print(col3)    


if __name__ == '__main__':
    col1 = Color([0.5, 0.5, 0.5])
    col2 = Color([0.5, 0.5, 0.5])

    #print(col1)
    #print(col2)

    print(add(col1, col2, 1))
    print(col1)
    print(col2)
