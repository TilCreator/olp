#!/usr/bin/env python
import os
import json
import logging
import asyncio
import websockets
import pprint
import fcntl
import termios
import struct

try:
    th, tw, hp, wp = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
except IOError:
    th, tw = 80, 200

pp = pprint.PrettyPrinter(indent=1, width=tw)

logger = logging.getLogger('websockets')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


async def cmd(c, cmd, **data):
    in_data = {'cmd': cmd, **data}

    print('-' * 20 + '\n' + 'IN: ')
    pp.pprint(in_data)
    print('OUT: ')

    await c.send(json.dumps(in_data))

    out_data = json.loads(await c.recv())
    pp.pprint(out_data)

    return out_data


async def test(uri):
    async with websockets.connect(uri) as c:
        await cmd(c, 'get_devices')
        await cmd(c, 'get_device', name='virt')

        await cmd(c, 'set_device', name='virt', data={'config': {'length': "11"}})
        await cmd(c, 'set_device', name='virt', data={'animation': {'speed': 3, 'name': 'test', 'brightness': 1}})
        await cmd(c, 'set_device', name='virt', data={'active': True})
        await cmd(c, 'change_device_name', old_name='virt', new_name='tmp')
        await cmd(c, 'remove_device', name='tmp')
        await cmd(c, 'add_device', name='virt', data={'config': {'active': True, 'delay': '0.5', 'driver': "virtual", 'length': '9'}, 'animation': {'speed': 3, 'name': 'test', 'brightness': 1}, 'active': True})

        await cmd(c, 'get_effects')
        await cmd(c, 'get_effect', name='test')
        await cmd(c, 'set_effect', name='test', data={
            "description": "Just a very simple test",
            "processes": {
                "id_constant": {
                    "component": "constant"
                },
                "id_out": {
                    "component": "out"
                }
            },
            "connections": [
                {
                    "src": {
                        "process": "id_constant",
                        "port": "colors"
                    },
                    "tgt": {
                        "process": "id_out",
                        "port": "leds"
                    }
                }
            ],
            "static_vars": {}
        })
        await cmd(c, 'add_effect', name='test2', data={
            "description": "Just a very simple test",
            "processes": {
                "id_constant": {
                    "component": "constant"
                },
                "id_out": {
                    "component": "out"
                }
            },
            "connections": [
                {
                    "src": {
                        "process": "id_constant",
                        "port": "colors"
                    },
                    "tgt": {
                        "process": "id_out",
                        "port": "leds"
                    }
                }
            ],
            "static_vars": {}
        })
        await cmd(c, 'test_effect', data={
            "description": "Just a very simple test",
            "processes": {
                "id_constant": {
                    "component": "constant"
                },
                "id_out": {
                    "component": "out"
                }
            },
            "connections": [
                {
                    "src": {
                        "process": "id_constant",
                        "port": "colors"
                    },
                    "tgt": {
                        "process": "id_out",
                        "port": "leds"
                    }
                }
            ],
            "static_vars": {}
        })
        await cmd(c, 'test_effect', data={
            "description": "Just a very simple test",
            "processes": {
                "id_constant": {
                    "component": "constant"
                },
                "id_out": {
                    "component": "out"
                },
                "id_out2": {
                    "component": "out"
                }
            },
            "connections": [
                {
                    "src": {
                        "process": "id_constant",
                        "port": "colors"
                    },
                    "tgt": {
                        "process": "id_out",
                        "port": "leds"
                    }
                }
            ],
            "static_vars": {
                "test": ["Number", "1"]
            }
        })
        await cmd(c, 'change_effect_name', old_name='test2', new_name='tmp')
        await cmd(c, 'remove_effect', name='tmp')

        await cmd(c, 'get_globals')
        await cmd(c, 'set_global', name='test', data='42')
        await cmd(c, 'remove_global', name='foo')
        await cmd(c, 'add_global', name='bar', data='3.14', type='Float')
        await cmd(c, 'change_global_name', old_name='bar', new_name='pi')

        await cmd(c, 'get_data_types')
        await cmd(c, 'get_nodes')
        await cmd(c, 'get_drivers')

        input('stop server now (and press enter)')

    os.rename('config.bak.json', 'config.json')
    os.rename('effects.bak.json', 'effects.json')
    os.rename('cache.bak.json', 'cache.json')
    os.rename('global_vars.bak.json', 'global_vars.json')


os.rename('config.json', 'config.bak.json')
with open('config.json', 'w') as f:
    f.write(json.dumps({
        'log_level': 'DEBUG',
        'webserver': False,
        'host': '127.0.0.1',
        'name': 'testing',
        'port': 6410,
        'ws_host': '127.0.0.1',
        'ws_port_server': 8785,
        'ws_port_client': 8785,
        'devices': {
            'virt': {
                'active': False,
                'delay': 0.5,
                'driver': "virtual",
                'length': 10
            }
        }
    }) + '\n')
os.rename('effects.json', 'effects.bak.json')
with open('effects.json', 'w') as f:
    f.write(json.dumps({
        "test": {
            "description": "Just a very simple test",
            "processes": {
                "id_rainbow": {
                    "component": "rainbow"
                },
                "id_out": {
                    "component": "out"
                }
            },
            "connections": [
                {
                    "src": {
                        "process": "id_rainbow",
                        "port": "leds"
                    },
                    "tgt": {
                        "process": "id_out",
                        "port": "leds"
                    }
                }
            ],
            "static_vars": {
                "test": ["Number", 42]
            }
        }
    }) + '\n')
os.rename('global_vars.json', 'global_vars.bak.json')
with open('global_vars.json', 'w') as f:
    f.write(json.dumps({
        "test": ['Number', 1],
        "foo": ['Float', 3.14]
    }) + '\n')
os.rename('cache.json', 'cache.bak.json')

input('start server now (and press enter)')

asyncio.get_event_loop().run_until_complete(test('ws://127.0.0.1:8785'))
