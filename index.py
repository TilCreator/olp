import os
import json
import time
import copy
import atexit
import logging
import threading
import traceback
import http.server
import urllib.request
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

import nodes
from drivers import drivers
from color import Color

pwd = os.path.dirname(os.path.realpath(__file__))

import webui.gen

os.chdir(pwd)

try:
    with open('config.json') as f:
        config = json.load(f)
except FileNotFoundError:
    print('Config not found, please create config.json (use config.example.json as template)')
    exit()

cache = effects = global_vars = {}
for file in ['cache', 'effects', 'global_vars']:
    try:
        with open(f'{file}.json') as f:
            globals()[file] = json.load(f)

            if file == 'global_vars':
                for key, val in global_vars.items():
                    if 'json_in_function' in nodes.data_types[val[0]]:
                        val[1] = nodes.data_types[val[0]]['json_in_function'](val[1])
    except FileNotFoundError:
        pass

nodes.update_global_vars(global_vars)


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=getattr(logging, config['log_level']))

logger = logging.getLogger(__name__)


def save_data(files=['config', 'effects', 'cache', 'global_vars']):
    if 'cache' in files:
        for key in devices:
            cache[key] = devices[key].animation

    for file in files:
        data = copy.deepcopy(globals()[file])

        if file == 'global_vars':
            for key, val in data.items():
                if 'json_out_function' in nodes.data_types[val[0]]:
                    val[1] = nodes.data_types[val[0]]['json_out_function'](val[1])

        with open(f'{file}.json', 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)


class Device():
    def __init__(self, name, config, devices):
        self.name = name
        self.config = config.copy()
        self.config['name'] = name
        self.leds = [Color() for _ in range(config['length'])]
        self.animation = {'name': None, 'active': False, 'state': 0, 'delta_time': 0, 'speed': 0, 'brightness': 0, 'brightnessSpeed': 0}
        self.effect = None
        self.last_error = None

        try:
            if 'init_with_devices' in drivers[config['driver']].keys() and drivers[config['driver']]['init_with_devices']:
                self.driver = drivers[config['driver']]['class'](self.config, devices)
            else:
                self.driver = drivers[config['driver']]['class'](self.config)
        except Exception as e:
            self.handel_error(e, 'Driver of device "{}" crashed'.format(self.name))

    def handel_error(self, e, msg=None):
        if msg is None:
            msg = f'Error {repr(e)} in device "{self.name}"'

        logger.exception(msg)
        self.last_error = {'long_error': traceback.format_exc(), 'short_error': repr(e), 'node_id': None}

        self.animation['active'] = False

        if hasattr(self, 'thread'):
            try:
                self.thread.join()
            except RuntimeError:
                pass
            except Exception as e:
                logger.exception(f'Exception {repr(e)} while closing thread of device "{self.name}"')

            del self.thread

        try:
            self.driver.close()
        except AttributeError:
            pass
        except Exception as e:
            logger.exception(f'Exception {repr(e)} while closing ghost of device "{self.name}"')

        send_message({'cmd': 'error_notification', 'text': f'Error: Device "{self.name}" crashed<br><br><pre>{self.last_error["short_error"]}</pre>'})
        send_message({'cmd': 'get_device', 'name': self.name, 'data': device_to_api(self)})

    def init_effect(self, effect):
        self.animation['name'] = effect

        if effect is None:
            self.effect = None
            return

        try:
            self.effect = nodes.effect(effects[effect], self.config)
        except Exception as e:
            self.handel_error(e, 'Init of device "{}" failed'.format(self.name))
        else:
            self.last_error = None

    def run(self):
        try:
            self.driver.open()
        except Exception as e:
            self.handel_error(e, f'Exception while opening driver of device "{self.name}"')

            return

        self.thread = threading.Thread(target=self._run)
        self.thread.daemon = True

        self.animation['active'] = True

        self.last_error = None

        self.thread.start()

        logger.info('Started daemon thread of device "{}"'.format(self.name))

    def stop(self):
        self.animation['active'] = False

        self.thread.join()
        del self.thread

        try:
            self.driver.close()
        except Exception as e:
            self.handel_error(e, f'Exception while closing driver of device "{self.name}"')

            return

        logger.info('Stopped daemon thread of device "{}"'.format(self.name))

    def _run(self):
        last_update = time.time()

        try:
            while self.animation['active']:
                if config['debug_devices']:
                    logger.debug(self.name + ': ' + str(self.animation))

                run_duration = time.time()

                if self.effect is not None:
                    self.leds, self.animation['speed'], self.animation['state'], self.animation['brightnessSpeed'], self.animation['brightness'] = \
                    self.effect.run(self.animation['speed'], self.animation['brightnessSpeed'], self.animation['state'], self.animation['brightness'], self.animation['delta_time']).values()

                self.driver.show(self.leds, self.animation)

                self.animation['delta_time'] = (time.time() - last_update)

                self.animation['state'] = (self.animation['state'] + self.animation['speed'] * self.animation['delta_time']) % 1

                self.animation['brightness'] = min(max((self.animation['brightness'] + self.animation['brightnessSpeed'] * (time.time() - last_update)), 0), 1)

                if config['debug_devices']:
                    logger.debug(self.name + ': delta_time: ' + str(time.time() - last_update) + ': ' + str(self.config['delay'] - (time.time() - last_update)))

                last_update = time.time()

                time.sleep(max(self.config['delay'] - (time.time() - run_duration), 0))
        except Exception as e:
            self.handel_error(e, 'Daemon thread of device "{}" crashed'.format(self.name))


def device_to_api(device):
    args = drivers[device.config['driver']]['args']
    args.update({'delay': 'Float', 'length': 'Number'})

    conf = {}
    for key, type in args.items():
        conf[key] = nodes.data_types[type]['out_function'](device.config[key])

    conf['driver'] = device.config['driver']

    error = {}
    if device.last_error is not None:
        error = {'error': device.last_error}

    return {'config': conf, 'leds': nodes.data_types['Colors']['out_function'](device.leds), 'animation': device.animation, **error}


clients = []
class WS_Server(WebSocket):
    def sendMessage(self, msg, **args):
        logger.debug(f'WS: Sending msg "{msg}" to {self.address}')
        super().sendMessage(json.dumps(msg), **args)

    def handleMessage(self):
        try:
            try:
                data = json.loads(self.data)
            except json.decoder.JSONDecodeError:
                logger.debug(f'WS: Received msg "{self.data}" from {self.address}')

                self.sendMessage({'error': 'Expecting json'})
                return

            logger.debug(f'WS: Received msg "{data}" from {self.address}')

            if 'cmd' not in data:
                self.sendMessage({'error': 'Expecting "cmd" key'})
                return
            else:
                try:
                    response = {'cmd': data['cmd']}
                    if data['cmd'] == 'get_devices':
                        response['devices'] = {key: device_to_api(device) for key, device in devices.items()}
                    elif data['cmd'] == 'get_device':
                        response['name'] = data['name']

                        device = devices[data['name']]
                        response['data'] = device_to_api(device)
                    elif data['cmd'] == 'set_device':
                        response['name'] = data['name']

                        device = devices.pop(data['name'])

                        driver = device.config['driver']
                        if 'config' in data['data'] and 'driver' in data['data']['config'] and data['data']['config']['driver'] is not None:
                            driver = data['data']['config']['driver']

                        cache[data['name']] = device.animation
                        if device.animation['active']:
                            device.stop()
                        del device

                        if 'config' in data['data']:
                            args = drivers[driver]['args']
                            args.update({'delay': 'Float', 'length': 'Number'})

                            for key, type in args.items():
                                if key in data['data']['config']:
                                    config['devices'][data['name']][key] = nodes.data_types[type]['in_function'](data['data']['config'][key])
                                elif key in config['devices'][data['name']]:
                                    pass
                                else:
                                    config['devices'][data['name']][key] = nodes.data_types[type]['default_value']

                            config['devices'][data['name']]['driver'] = driver

                            if 'name' in config['devices'][data['name']]:
                                del config['devices'][data['name']]['name']

                        if 'animation' in data['data']:
                            cache[data['name']].update(data['data']['animation'])

                            if data['data']['animation']['name'] is None:
                                cache[data['name']]['name'] = None

                        devices[data['name']] = Device(data['name'], config['devices'][data['name']], devices)
                        device = devices[data['name']]

                        device.animation = cache[data['name']]

                        if device.animation['name'] is not None:
                            device.init_effect(device.animation['name'])

                        if device.animation['active']:
                            device.run()

                        response['data'] = device_to_api(device)

                        save_data(['config', 'cache'])
                    elif data['cmd'] == 'remove_device':
                        device = devices.pop(data['name'])
                        config['devices'].pop(data['name'])

                        if device.animation['active']:
                            device.stop()
                        del device

                        response['devices'] = {key: device_to_api(device) for key, device in devices.items()}

                        save_data(['config', 'cache'])
                    elif data['cmd'] == 'add_device':
                        if data['name'] in devices.keys():
                            self.sendMessage({'error': f'Device already exists'})
                            return

                        args = drivers[data['data']['config']['driver']]['args']
                        args.update({'delay': 'Float', 'length': 'Number'})

                        conf = {}
                        conf['driver'] = data['data']['config']['driver']

                        for key, type in args.items():
                            if key in data['data']['config']:
                                conf[key] = nodes.data_types[type]['in_function'](data['data']['config'][key])
                            else:
                                conf[key] = nodes.data_types[type]['default_value']

                        config['devices'][data['name']] = conf

                        devices[data['name']] = Device(data['name'], config['devices'][data['name']], devices)
                        device = devices[data['name']]

                        if 'animation' in data['data']:
                            device.animation.update(data['data']['animation'])

                        if device.animation['name'] is not None:
                            device.init_effect(device.animation['name'])

                        if device.animation['active']:
                            device.run()

                        device = devices[data['name']]
                        response['devices'] = {key: device_to_api(device) for key, device in devices.items()}

                        save_data(['config', 'cache'])
                    elif data['cmd'] == 'change_device_name':
                        devices[data['new_name']] = devices.pop(data['old_name'])
                        config['devices'][data['new_name']] = config['devices'].pop(data['old_name'])

                        response['devices'] = {key: device_to_api(device) for key, device in devices.items()}

                        save_data(['config', 'cache'])
                    elif data['cmd'] == 'get_effects':
                        response['effects'] = {key: {'description': effect['description']} for key, effect in effects.items()}
                    elif data['cmd'] == 'get_effect':
                        effect = effects[data['name']]

                        response['name'] = data['name']
                        response['data'] = {key: effect[key] for key in ['description', 'processes', 'connections', 'static_vars']}
                        response['data']['caseSensitive'] = True

                        self.sendMessage(response)
                        return
                    elif data['cmd'] in 'set_effect':
                        effect = effects[data['name']]

                        tmp_devices = []
                        for _, device in devices.items():
                            if device.animation['name'] == data['name'] and device.animation['active']:
                                device.stop()
                                tmp_devices.append(device)

                        effect.update({key: data['data'][key] for key in ['description', 'processes', 'connections', 'static_vars']})

                        for device in tmp_devices:
                            device.init_effect(data['name'])
                            device.run()

                        response['effects'] = {key: {'description': effect['description']} for key, effect in effects.items()}

                        save_data(['config', 'effects', 'cache'])
                    elif data['cmd'] in 'remove_effect':
                        for _, device in devices.items():
                            if device.animation['name'] == data['name']:
                                if device.animation['active']:
                                    device.stop()

                                device.init_effect(None)

                        effects.pop(data['name'])

                        response['effects'] = {key: {'description': effect['description']} for key, effect in effects.items()}

                        save_data(['config', 'effects', 'cache'])
                    elif data['cmd'] == 'add_effect':
                        if data['name'] in effects.keys():
                            self.sendMessage({'error': f'Effect already exists'})
                            return

                        effects[data['name']] = {
                            'connections': [],
                            'description': '',
                            'processes': {},
                            'static_vars': {}
                        }

                        response['effects'] = {key: {'description': effect['description']} for key, effect in effects.items()}

                        save_data(['effects'])
                    elif data['cmd'] == 'change_effect_name':
                        tmp_devices = []
                        for _, device in devices.items():
                            if device.animation['name'] == data['old_name'] and device.animation['active']:
                                device.stop()
                                tmp_devices.append(device)

                        effects[data['new_name']] = effects.pop(data['old_name'])

                        for device in tmp_devices:
                            device.init_effect(data['new_name'])
                            device.start()

                        response['effects'] = {key: {'description': effect['description']} for key, effect in effects.items()}

                        save_data(['config', 'effects', 'cache'])
                    elif data['cmd'] == 'test_effect':
                        try:
                            effect = nodes.effect(data['data'], {
                                'delay': 0,
                                'driver': None,
                                'length': 10,
                                'name': 'testing'
                            })

                            animation = {'name': 'to_test_effect', 'active': True, 'state': 0, 'delta_time': 0.01, 'speed': 1, 'brightness': .5, 'brightnessSpeed': 0}

                            for _ in range(0, 101):
                                _, animation['speed'], animation['state'], animation['brightnessSpeed'], animation['brightness'] = \
                                effect.run(animation['speed'], animation['brightnessSpeed'], animation['state'], animation['brightness'], animation['delta_time']).values()

                                time.sleep(0.01)
                        except Exception as e:
                            response['pass'] = False
                            response['nodes_error'] = {'long_error': traceback.format_exc(), 'short_error': repr(e), 'node_id': None}
                        else:
                            response['pass'] = True

                        self.sendMessage(response)
                        return
                    elif data['cmd'] == 'get_globals':
                        response['globals'] = {key: [global_vars[key][0], nodes.data_types[global_vars[key][0]]['out_function'](global_vars[key][1])] for key in global_vars}
                    elif data['cmd'] == 'set_global':
                        global_vars[data['name']][1] = nodes.data_types[global_vars[data['name']][0]]['in_function'](data['data'])

                        response['globals'] = {key: [global_vars[key][0], nodes.data_types[global_vars[key][0]]['out_function'](global_vars[key][1])] for key in global_vars}

                        save_data(['global_vars'])
                        nodes.update_global_vars(global_vars)
                    elif data['cmd'] == 'add_global':
                        if 'data' in data:
                            global_vars[data['name']] = [data['type'], nodes.data_types[data['type']]['in_function'](data['data'])]
                        else:
                            global_vars[data['name']] = [data['type'], nodes.data_types[data['type']]['default_value']]

                        response['globals'] = {key: [global_vars[key][0], nodes.data_types[global_vars[key][0]]['out_function'](global_vars[key][1])] for key in global_vars}

                        save_data(['global_vars'])
                        nodes.update_global_vars(global_vars)
                    elif data['cmd'] == 'remove_global':
                        global_vars.pop(data['name'])

                        response['globals'] = {key: [global_vars[key][0], nodes.data_types[global_vars[key][0]]['out_function'](global_vars[key][1])] for key in global_vars}

                        save_data(['global_vars'])
                        nodes.update_global_vars(global_vars)
                    elif data['cmd'] == 'change_global_name':
                        global_vars[data['new_name']] = global_vars.pop(data['old_name'])

                        response['globals'] = {key: [global_vars[key][0], nodes.data_types[global_vars[key][0]]['out_function'](global_vars[key][1])] for key in global_vars}

                        save_data(['global_vars'])
                        nodes.update_global_vars(global_vars)
                    elif data['cmd'] == 'get_data_types':
                        response['data_types'] = {key: val['html'] for key, val in nodes.data_types.items()}
                    elif data['cmd'] == 'get_nodes':
                        response['nodes'] = nodes.get_available()
                    elif data['cmd'] == 'get_drivers':
                        default_args = {'delay': ['Float', '0.03'], 'length': ['Number', '10']}

                        response['drivers'] = {}
                        for key, val in drivers.items():
                            args = {name: [type, nodes.data_types[type]['default_value']] for name, type in val['args'].items()}
                            args.update(default_args)
                            response['drivers'][key] = {'name': val['name'], 'description': val['description'], 'args': args}
                    else:
                        self.sendMessage({'error': f'Unknown cmd "{data["cmd"]}"'})
                except KeyError as e:
                    logging.exception('WS handeling: KeyError')
                    self.sendMessage({'error': f'Expecting "{e}" key'})
                    return

            send_message(response)
        except Exception:
            logging.exception('WS handeling')
            self.close(status=1011, reason='Internal server error')

    def handleConnected(self):
        logger.info(f'WS: "{self.address}" connected')
        clients.append(self)

    def handleClose(self):
        logger.info(f'WS: "{self.address}" disconnected')
        clients.remove(self)


def send_message(msg):
    for client in clients:
        thread = threading.Thread(target=lambda msg: client.sendMessage(msg), kwargs={'msg': msg})
        thread.daemon = True
        thread.run()


ws_server = SimpleWebSocketServer(config['ws_host'], config['ws_port_server'], WS_Server)


def run_webserver():
    global httpd

    class Handler(http.server.SimpleHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, directory=os.path.join(os.path.dirname(__file__), 'webui', 'build'), **kwargs)

        def log_message(self, format, *args):
            logger.info(f'webserver - {self.address_string()} - {format%args}')

    class Server(http.server.ThreadingHTTPServer):
        def log_message(self, format, *args):
            logger.info(f'webserver - {format%args}')

    httpd = Server((config['host'], config['port']), Handler)

    logger.info(f'Started webserver on {config["host"]}:{config["port"]}')

    while config['webserver']:
        httpd.handle_request()

    logger.info(f'Stopped webserver on {config["host"]}:{config["port"]}')


if __name__ == '__main__':
    devices = {}
    for key in config['devices']:
        devices[key] = Device(key, config['devices'][key], devices)

        if key in cache:
            devices[key].animation = cache[key]

            if devices[key].animation['name'] is not None:
                devices[key].init_effect(devices[key].animation['name'])

        if devices[key].animation['active']:
            devices[key].run()

    if config['webserver']:
        webserver_thread = threading.Thread(target=run_webserver)
        webserver_thread.daemon = True
        webserver_thread.start()

    @atexit.register
    def at_last():
        if config['webserver']:
            config['webserver'] = False

            urllib.request.urlopen(f'http://{config["host"]}:{config["port"]}').read()

            webserver_thread.join()

            config['webserver'] = True

        for key in devices:
            cache[key] = devices[key].animation

        save_data()
        logger.info('Saved config, effects and cache')

        for key in devices:
            if devices[key].animation['active']:
                devices[key].stop()

        logger.info('Stopped lights')

    try:
        ws_server.serveforever()
    except KeyboardInterrupt:
        exit()
