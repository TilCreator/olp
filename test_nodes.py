import nodes

raw_effect = {
    "processes": {
        "id_rainbow": {
            "component": "rainbow"
        },
        "id_out": {
            "component": "out"
        },
        "id_add": {
            "component": "test_function_0"
        },
        "id_add2000": {
            "component": "test_function_1"
        },
        "id_in": {
            "component": "in"
        },
        "id_Float": {
            "component": "Float"
        },
        "id_static": {
            "component": "globals"
        }
    },
    "connections": [
        {
            "src": {
                "process": "id_rainbow",
                "port": "leds"
            },
            "tgt": {
                "process": "id_out",
                "port": "leds"
            }
        },
        {
            "src": {
                "process": "id_add",
                "port": "result"
            },
            "tgt": {
                "process": "id_rainbow",
                "port": "state"
            }
        },
        {
            "src": {
                "process": "id_in",
                "port": "state"
            },
            "tgt": {
                "process": "id_add",
                "port": "number1"
            }
        },
        {
            "src": {
                "process": "id_Float",
                "port": "Float"
            },
            "tgt": {
                "process": "id_add2000",
                "port": "number1"
            }
        },
        {
            "src": {
                "process": "id_add2000",
                "port": "result"
            },
            "tgt": {
                "process": "id_add",
                "port": "number2"
            }
        },
        {
            "src": {
                "process": "id_static",
                "port": "global_num"
            },
            "tgt": {
                "process": "id_add2000",
                "port": "number2"
            }
        },
        {
            "src": {
                "process": "id_add",
                "port": "result"
            },
            "tgt": {
                "process": "id_out",
                "port": "state"
            }
        }
    ],
    "static_vars": {
        "id_Float": "0.1"
    }
}

config = {
    'length': 10
}

global_vars = {
    'global_num': ['Float', 0]
}

if __name__ == '__main__':
    import pprint
    from time import time
    import fcntl
    import termios
    import struct
    
    try:
        th, tw, hp, wp = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
    except IOError:
        th, tw = 80, 200
    
    pp = pprint.PrettyPrinter(indent=1, width=tw)

    nodes.update_global_vars(global_vars)

    print('# nodes')
    pp.pprint(nodes.nodes)

    print('# data_types')
    pp.pprint(nodes.data_types)

    print('-' * 20 + '\n' + '# nodes_web')
    pp.pprint(nodes.get_available())

    print('-' * 20 + '\n' + '# preinterpret')
    last_time = time()
    test_pre = nodes.effect(raw_effect, config)
    pp.pprint(test_pre.prepeared_node_list)
    print('time:', time() - last_time)

    print('-' * 20 + '\n' + '# run')
    for i in range(11):
        i = i / 10
        if i < 5: j = -0.1
        else: j = 0

        last_time = time()

        print('state:', i, 'global_num:', j)

        global_vars['global_num'][1] = j

        pp.pprint(test_pre.run(speed=0, brightnessSpeed=0, state=i, brightness=.5))

        print('time:', time() - last_time)

        print('-' * 5)
