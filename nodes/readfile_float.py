def readfile_float(config, animation, path='/dev/null'):
    with open(path, 'r') as f:
        content = f.read()
        if len(content) > 0:
            num = float(content)
        else:
            num = 0
        return {'float': num}


attributes = {
    'in': {'path': 'String'},
    'out': {'float': 'Float'},
    'function': readfile_float,
    'type': 'input',
    'name': 'Read file to float',
    'description': 'Reads a file and parses it as a float'
}
