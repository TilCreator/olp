class counter():
    def __init__(self, config):
        self.config = config
        self.count = 0

    def run(self, config, animation, speed=1):
        self.count += speed

        return {'count': self.count}


attributes = {
    'in': {'speed': 'Float'},
    'out': {'count': 'Float'},
    'class': counter,
    'type': 'math',
    'name': 'counter',
    'description': 'Counter, can be used with modulo to create idependent state.'
}
