import color
import random
droplets = []

import math

class LavaDroplet():

    def __init__(self, iniX, iniY, size, forceX, forceY):
        self.x = iniX
        self.y = iniY
        self.size = size / 2
        self.forceY = forceX
        self.forceX = forceY

    def update(self, deltaTime, speed):
        self.x = ((self.x + (self.forceX*deltaTime*speed) + 5) % 20) - 5
        self.y = (self.y + self.forceY*deltaTime) % 8

    def getDistance(self, x):
        distance = math.sqrt(((4.0-self.y) * (4.0-self.y)) + ((x-self.x) * (x-self.x)))

        if (distance < self.size):
            return 1.0

        if (distance < self.size + 0.8):
            return (distance - self.size) * (-1)+1

        return 0.2


def animation(config, animation, startHue = 0, hueShift = False, dropletCount = 20, color1 = None, color2 = None, sizeMin = 1, sizeMax = 3):
    # config = {'active': True, 'delay': 0.01, 'driver': 'wifiWS2', 'length': 150, 'ip': '151.217.161.139', 'port': 2342}
    # animation = {'name': 'test', 'state': 0.3729264497757008, 'speed': 0.5, 'brightness': 1.0, 'brightnessSpeed': 0, 'args': {'colors': [[1, 1, 1], [0, 0, 0]]}}
    leds = []
    if (hueShift and not (color1!=None and color2!=None)):
        startHue = animation['state']+startHue
    if (len(droplets)==0):
        for i in range (dropletCount):
            droplets.append(LavaDroplet(random.random()*20-5,
                                        random.random()*8,
                                        sizeMin + (random.random()*(sizeMax-sizeMin)),
                                        random.random()*2,
                                        random.random()*2))
    for i in range(config['length']):
        x = i / config['length']*10
        distance = droplets[0].getDistance(x)
        reference = 0
        for n in range (len(droplets)):
            reference = droplets[n].getDistance(x)
            if (reference>distance):
                distance = reference

        if (color1==None or color2==None):
            leds.append(color.Color([startHue + (distance*0.2), 1, distance*0.5+0.5],3))
        else:
            leds.append(color.mix(color1, color2, (distance-0.2)*1.25))

    # leds = [(0, 0, 0), (1, 1, 1), ...]
    for i in range(dropletCount):
        droplets[i].update(animation['delta_time'], animation['speed'])

    return {'leds': leds}


attributes = {
    'name': 'LavaLampe',
    'description': 'Makes a lava lamp. You can set hue to a starting value. (0 by Default) You can set hueShift to make the hue shift. (Default: False) You can also set the number of total droplets (Default 20). If two Colors are set, the starting hue and hue shift are ignored. (Not set by default)',
    'type': 'animation',
    'function': animation,
    'in': {'startHue': 'Float', 'hueShift': 'Bool', 'dropletCount': 'Number', "color1": 'Color', "color2": 'Color', 'sizeMin': 'float','sizeMax': 'float'},
    'out': {'leds': 'Colors'}
}

#attributes = {
#    'name': 'rainbow',
#    'description': 'Rainbow animation',
#    'type': 'animation',
#    'function': animation,
#    'in': {'state': 'Float', 'length': 'Number', 'saturation': 'Float', 'scale': "Float"},
#    'out': {'leds': 'Colors'}
#}
