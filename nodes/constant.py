from color import Color
from copy import deepcopy


def constant(config, animation, color=Color()):
    return {'colors': [deepcopy(color) for _ in range(config['length'])]}


attributes = {
    'in': {'color': 'Color'},
    'out': {'colors': 'Colors'},
    'function': constant,
    'type': 'converter',
    'name': 'constant',
    'description': 'Convert Color to Colors'
}
