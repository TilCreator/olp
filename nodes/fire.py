from noise import pnoise2
from color import Color
from copy import deepcopy

class fire():
    def __init__(self, config):
        self.config = config
        self.y = 0

    def run(self, config, animation, scale=20, color1=Color([1, 0.4, 0]), color2=Color([1, 0.1, 0])):
        self.y += animation['delta_time'] * animation['speed'] * 5
        noise = [pnoise2(x / self.config['length'] * scale, self.y, octaves=1, persistence=0.5) for x in range(101, int(101 + self.config['length']))]

        colors = [deepcopy(color1) for _ in noise]
        [colors[i].mix(color2, val) for i, val in enumerate(noise)]

        return {'colors': colors}


attributes = {
    'in': {'scale': 'Float', 'color1': 'Color', 'color2': 'Color'},
    'out': {'colors': 'Colors'},
    'class': fire,
    'type': 'animation',
    'name': 'fire',
    'description': 'Fire animation with perl noise'
}
