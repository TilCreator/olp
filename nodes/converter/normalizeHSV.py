def normalizeHSV(hsv):
    """
    Applies modulo to hue, Caps saturation at 1, and caps value at 1.
    Caps saturation and value below 0.
    """
    hsv[0] = hsv[0]%1
    if hsv[1] > 1: hsv[1] = 1
    if hsv[2] > 1: hsv[2] = 1
    if hsv[1] < 0: hsv[1] = 0
    if hsv[2] < 0: hsv[2] = 0

    return hsv

def normalizeHsvArray(hsvArray):
    """
    Overrides RGB values of Array 1 with RGB values of Arrays2 where rgb2[i]!=(0,0,0)
    Set flag true if you want to override a HSL Array where hsl2[i]!=(*,*,0)
    Caps saturation and value below 0.
    """

    for i in hsvArray:
        i = normalizeHSV(i)
    return hsvArray

config = [{
    'in': {'rgb1': 'Color', 'rgb2': 'Color', 'fac': 'Number'},
    'out': {'result': 'Color'},
    'function': normalizeHSV,
    'name': 'normalize HSV',
    'icon': '/icons/penisland'
},
{
    'in': {'rgbArray1': 'Color[]', 'rgbArray2': 'Color[]', 'fac': 'Number'},
    'out': {'result': 'Color[]'},
    'function': normalizeHsvArray,
    'name': 'normalize HSV',
    'icon': '/icons/penisland'
}]