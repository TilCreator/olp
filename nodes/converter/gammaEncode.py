def gammaEncode(rgb, gamma=2.2):
    return (rgb ^ (1 / gamma))

def gammaEncodeArray(rgbArray, gamma=2.2):
    for i in rgbArray:
        i = gammaEncode(i, gamma)
    return rgbArray

def gammaDecode(rgb, gamma=2.2):
    return (rgb ^ gamma)

def gammaDecodeArray(rgbArray, gamma=2.2):
    for i in rgbArray:
        i = gammaDecode(i, gamma)
    return rgbArray

config = [{
    'in': {'rgb': 'Color', 'gamma': 'Number'},
    'out': {'result': 'Color'},
    'function': gammaEncode,
    'name': 'encode Gamma',
    'icon': '/icons/penisland'
},{
    'in': {'rgb': 'Color', 'gamma': 'Number'},
    'out': {'result': 'Color'},
    'function': gammaDecode,
    'name': 'decode Gamma',
    'icon': '/icons/penisland'
},{
    'in': {'rgbArray': 'Color', 'gamma': 'Number'},
    'out': {'result': 'Color[]'},
    'function': gammaEncodeArray,
    'name': 'encode Gamma',
    'icon': '/icons/penisland'
},{
    'in': {'rgbArray': 'Color[]', 'gamma': 'Number'},
    'out': {'result': 'Color[]'},
    'function': gammaDecodeArray,
    'name': 'encode gamma',
    'icon': '/icons/penisland'
}]