from color import Color
import json


attributes = {
    'Float': {
        'html': '<input oninit="" type=number step=0.0001 class="out"/>',
        'in_function': lambda data: float(json.loads(data)),
        'out_function': lambda data: json.dumps(data),
        'default_function': lambda config: 0,
        'default_value': 0
    },
    'Number': {
        'html': '<input oninit="" type=number step=1 class="out"/>',
        'in_function': lambda data: int(json.loads(data)),
        'out_function': lambda data: json.dumps(data),
        'default_function': lambda config: 0,
        'default_value': 0
    },
    'Bool': {
        'html': '<input class="switch bool" type="checkbox" onclick="document.querySelector(`#${this.parentNode.parentNode.id} .value .out`).value = this.checked; document.querySelector(`#${this.parentNode.parentNode.id} .value .out`).onchange.call(document.querySelector(`#${this.parentNode.parentNode.id} .value .out`))"/>\
        <input oninit="document.querySelector(`#${this.parentNode.parentNode.id} .value .bool`).checked = JSON.parse(this.value)" type=text class="out hidden"/>',
        'in_function': lambda data: bool(json.loads(data)),
        'out_function': lambda data: json.dumps(data),
        'default_function': lambda config: True,
        'default_value': True
    },
    'Color': {
        'html': '<input oninit="" type=color class="out">',
        'in_function': lambda data: Color(data[1:], mode=4),
        'out_function': lambda data: '#' + data.getHex(),
        'json_in_function': lambda data: Color(data, mode=1),
        'json_out_function': lambda data: data.getRgb(),
        'default_function': lambda config: Color(),
        'default_value': Color()
    },
    'Colors': {
        'html': '<input oninit="" type=text class="out">',
        'in_function': lambda data: [Color(color_data, mode='hex') for color_data in json.loads(data)],
        'out_function': lambda data: [color_data.getHex() for color_data in data],
        'json_in_function': lambda data: [Color(color_data, mode='rgb') for color_data in json.loads(data)],
        'json_out_function': lambda data: [color_data.getRgb() for color_data in data],
        'default_function': lambda config: [Color() for _ in range(config['length'])],
        'default_value': [Color()]
    },
    'String': {
        'html': '<input oninit="" type=text class="out"/>',
        'in_function': lambda data: str(data),
        'out_function': lambda data: data,
        'default_function': lambda config: '',
        'default_value': ''
    }
}
