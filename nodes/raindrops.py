from nodes.gradient import gradient
from color import Color
class drop():
    def __init__(self, direction, color, length, pos):
        self.direction = direction
        self.color = color
        if color.mode == 3: color._convert(1, True)
        self.length = length
        self.startlength = length
        self.pos = pos
        self.strength = length
        self.dead = False

    def update(self, animation):
        if self.direction:
            self.pos = self.pos + (animation['speed']*animation['deltatime'])
        else:
            self.pos = self.pos - (animation['speed']*animation['deltatime'])

        self.length = self.length - (animation['speed']*animation['deltatime'])

        if self.position > 1 or self.position<0: self.direction = not self.direction
        if self.length < 0 : self.dead = Trues


    def draw(self, pos):
        val = self._draw(pos)
        return Color(self.color._cmp, self.color.mode, self.color.gamma).multiply(Color([val,val,val]))

    def _draw(self, pos):
        if self.direction:
            if self.pos+0.2 > 1:
                return gradient(pos, self.pos-0.05, self.pos, True) + gradient(pos, self.pos+0.02, self.pos, False) + gradient(pos, 1-(self.pos+0.02)+1, self.pos, False) * self.length/self.startlength
            if self.pos-0.5 < 0:
                return gradient(pos, self.pos-0.05, self.pos, True) + gradient(pos, self.pos+0.02, self.pos, False) + gradient(pos, 0-(self.pos+0.05), self.pos, True) * self.length/self.startlength
            return  gradient(pos, self.pos-0.05, self.pos, True) + gradient(pos, self.pos+0.02, self.pos, False) * self.length /self.startlength
        else:
            if self.pos-0.2 < 1:
                return  gradient(pos, self.pos-0.05, self.pos, True) + gradient(pos, self.pos+0.02, self.pos, False) + gradient(pos, 0-(self.pos+0.02), self.pos, False) * self.length /self.startlength
            if self.pos+0.5 > 0:
                return  gradient(pos, self.pos-0.05, self.pos, True) + gradient(pos, self.pos+0.02, self.pos, False) + gradient(pos, 1-(self.pos+0.05)+1, self.pos, True) * self.length /self.startlength
            return  gradient(pos, self.pos+0.05, self.pos, True) + gradient(pos, self.pos-0.02, self.pos, False) * self.length /self.startlength

class raindrops():
    def __init__(self, config):
        self.config = self.config
        self.test = drop(True, Color([1,0,0]), 0.5, 0.8)

    def run(self, animation):
        self.test.update(animation)
        if self.test.dead: return
        col = []
        rgb = []
        for i in self.config['length']-1:
            col.append(self.test.draw())
            rgb.append(col[i].getRgbEncoded())



attributes = {
    'in': {'Droplet Count': 'Number', 'Color': 'Color', 'Length':'Float'},
    'out': {'result': 'Float'},
    'class': raindrops,
    'type': 'static converter',
    'name': 'counter',
    'description': 'Inputs one number, outputs sum of all inputs numbers'
}
# speed, deltatime, brighntess, state, length
