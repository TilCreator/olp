from color import Color

def dimColors(config, animation, colors=[Color()], factor=None):
    if factor is None: factor = animation['brightness']

    [color.dim(factor) for color in colors]

    return {'colors': colors}


def dimColor(config, animation, col=Color(), factor=None):
    if factor is None: factor = animation['brightness']

    col.dim(factor)

    return {'color': col}


def addColors(config, animation, colors1=[Color()], colors2=[Color()], factor=1, gammaCorrect = True):
    [colors1[i].add(colors2[i], factor, gammaCorrect) for i in range(config['length'])]

    return {'colors': colors1}


def addColor(config, animation, col1=Color(), col2=Color(), factor=1, gammaCorrect = True):
    col1.add(col2, factor, gammaCorrect)

    return {'color': col1}


def overlayColors(config, animation, colors1=[Color()], colors2=[Color()], factor=1):
    [colors1[i].overlay(colors2[i], factor) for i in range(config['length'])]

    return {'colors': colors1}


def overlayColor(config, animation, col1=Color(), col2=Color(), factor=1):
    col1.overlay(col2, factor)

    return {'color': col1}


def blendHues(config, animation, colors1=[Color()], colors2=[Color()], factor=1):
    [colors1[i].mixHue(colors2[i], factor) for i in range(config['length'])]

    return {'colors': colors1}


def blendHue(config, animation, col1=Color(), col2=Color(), factor=1):
    col1.mixHue(col2, factor)

    return {'color': col1}


def blendSaturations(config, animation, colors1=[Color()], colors2=[Color()], factor=1):
    [colors1[i].mixHue(colors2[i], factor) for i in range(config['length'])]

    return {'colors': colors1}


def blendSaturation(config, animation, col1=Color(), col2=Color(), factor=1):
    col1.mixHue(col2, factor)

    return {'color': col1}


def blendValues(config, animation, colors1=[Color()], colors2=[Color()], factor=1):
    [colors1[i].mixValue(colors2[i], factor) for i in range(config['length'])]

    return {'colors': colors1}


def blendValue(config, animation, col1=Color(), col2=Color(), factor=1):
    col1.mixValue(col2, factor)

    return {'color': col1}


def blendColors(config, animation, colors1=[Color()], colors2=[Color()], factor=0.5):
    [colors1[i].mixColor(colors2[i], factor) for i in range(config['length'])]

    return {'colors': colors1}


def blendColor(config, animation, col1=Color(), col2=Color(), factor=0.5):
    col1.mixColor(col2, factor)

    return {'color': col1}


def mixSingle(config, animation, colors1=[Color()], colors2=[Color()], factor=0.5, gammaCorrect = True):
    [colors1[i].mix(colors2[i], factor, gammaCorrect) for i in range(config['length'])]

    return {'colors': colors1}


def mixMultiple(config, animation, col1=Color(), col2=Color(), factor=0.5, gammaCorrect = True):
    col1.mix(col2, factor, gammaCorrect)

    return {'color': col1}


def multiplyColors(config, animation, colors1=[Color()], colors2=[Color()], factor=0.5, gammaCorrect = True):
    [colors1[i].multiply(colors2[i], factor, gammaCorrect) for i in range(config['length'])]

    return {'colors': colors1}


def multiplyColor(config, animation, col1=Color(), col2=Color(), factor=0.5, gammaCorrect = True):
    col1.multiply(col2, factor, gammaCorrect)

    return {'color': col1}


def overrideColors(config, animation, colors1=[Color()], colors2=[Color()]):
    [colors1[i].override(colors2[i]) for i in range(config['length'])]

    return {'colors': colors1}


def overrideColor(config, animation, col1=Color(), col2=Color()):
    col1.override(col2)

    return {'color': col1}


attributes = [{
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': dimColors,
    'type': 'mixModes',
    'name': 'Dim Colors',
    'description': 'Dimms Colors by a factor. Global brightness is default factor.'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': dimColor,
    'type': 'mixModes',
    'name': 'Dim Color',
    'description': 'Dimms a Color by a factor. Global brightness is default factor.'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': addColors,
    'type': 'mixModes',
    'name': 'Add Colors',
    'description': 'Adds Colors to a second array of Colors. You can determine the strength of the added Colors with a factor or set gamma correct to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'col': 'Color', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': addColor,
    'type': 'mixModes',
    'name': 'Add Color',
    'description': 'Adds a Color to another Color. You can determine the strength of the added Color with a factor or set gammaCorrect to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': overlayColors,
    'type': 'mixModes',
    'name': 'Overlay Colors',
    'description': 'Overlays a second set of colors onto the first set by the difference of the second color and 0.5. A factor can modify the strength of this effect.'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': overlayColor,
    'type': 'mixModes',
    'name': 'Overlay Color',
    'description': 'Overlays a second color onto the first by the difference of the second color and 0.5. A factor can modify the strength of this effect.'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendHues,
    'type': 'mixModes',
    'name': 'blend Hues',
    'description': 'Changes the hue of a first set of Colors to the hue of a second set of colors by a factor. (Default: 1)'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendHue,
    'type': 'mixModes',
    'name': 'Blend Hue',
    'description': 'Changes the hue of a first Color to the hue of a second one by a factor. (Default: 1)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendSaturation,
    'type': 'mixModes',
    'name': 'Blend Saturation',
    'description': 'Changes the saturation of a first set of Colors to the saturation of a second set of colors by a factor. (Default: 1)'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendSaturation,
    'type': 'mixModes',
    'name': 'Blend Saturation',
    'description': 'Changes the saturation of a first Color to the saturation of a second one by a factor. (Default: 1)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendValues,
    'type': 'mixModes',
    'name': 'Blend Value',
    'description': 'Changes the values of a first set of Colors to the values of a second set of colors by a factor. (Default: 1)'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendValue,
    'type': 'mixModes',
    'name': 'Blend Value',
    'description': 'Changes the value of a first Color to the value of a second one by a factor. (Default: 1)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendColors,
    'type': 'mixModes',
    'name': 'Blend Color',
    'description': 'Changes the saturations and hues of a first set of Colors to those of a second set of colors by a factor. (Default: 1)'
}, {
    'in': {'col': 'Color', 'factor': 'Float'},
    'out': {'colors': 'Colors'},
    'function': blendColor,
    'type': 'mixModes',
    'name': 'Blend Colors',
    'description': 'Changes the saturation and hue of a first Color to the values of a second Color by a factor. (Default: 1)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': mixMultiple,
    'type': 'mixModes',
    'name': 'Mix Colors',
    'description': 'Blends to sets of Colors by a factor. (Default: 0.5) Results will look diffrent depending on your Color Space (RGB/RGBW or HSV). Set gammaCorrect to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'col': 'Color', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': mixSingle,
    'type': 'mixModes',
    'name': 'Mix Color',
    'description': 'Blends two Colors by a factor. (Default: 0.5) Results will look diffrent depending on your Color Space (RGB/RGBW or HSV). Set gammaCorrect to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'colors': 'Colors', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': multiplyColors,
    'type': 'mixModes',
    'name': 'Multiply Colors',
    'description': 'Multiplies each component of two sets of Colors by a factor. (Default: 0.5) Set gammaCorrect to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'col': 'Color', 'factor': 'Float', 'gammaCorrect': 'Bool'},
    'out': {'colors': 'Colors'},
    'function': multiplyColor,
    'type': 'mixModes',
    'name': 'Multiply Color',
    'description': 'Multiplies each component of two Colors by a factor. (Default: 0.5) Set gammaCorrect to false to emulate a non-linear color space. (not reccomended)'
}, {
    'in': {'colors1': 'Colors', 'colors2': 'Colors'},
    'out': {'colors': 'Colors'},
    'function': overrideColors,
    'type': 'mixModes',
    'name': 'Override Colors',
    'description': 'Overrides a Color from a first set of Colors with a color from a second set, where the second color is not black.'
}, {
    'in': {'col': 'Color'},
    'out': {'colors': 'Colors'},
    'function': overrideColor,
    'type': 'mixModes',
    'name': 'Override Color',
    'description': 'Overrides a Color with another color, if the second color is not black.'
}]
