from color import Color
import copy


class pohotosensitivityBreak():
    def __init__(self, config):
        self.reference = []
        self.new = True
        self.rating = 0

    def run(self, config, animation, colors=[Color()], threshold = None, response = None):
        if threshold == None: threshold = 0
        if response == None: response = 0.1

        rating = 0

        if self.new:
            for i in range(config['length']):
                self.reference.append(Color([0.5,0.5,0.5]))
                
            self.new = False
        else:
            for i in range(config['length']):
                hsvRef = self.reference[i].getHsv()
                hsvNew = colors[i].getHsv()
                rating = rating + (abs(hsvRef[0]-hsvNew[0]) + abs(hsvRef[1]-hsvNew[1]) + abs(hsvRef[2]-hsvNew[2]))*hsvNew[2]
            rating = (((rating / config['length']) - threshold) * response)

        self.rating = self.rating + rating

        print (self.rating)

        if self.rating > 1:
            self.rating = 1
        if self.rating < -1:
            self.rating = -1

        print (self.rating)
                
        for i in range(config['length']):
            self.reference[i].mix(colors[i], 0.7)
            #print(self.reference[i])
        if self.rating <= 0:
            return {'output': colors, 'isEngaged': (rating>0)}
        else:
            for i in range(config['length']):
                if (i < config['length']*0.1) or (i > config['length']*0.9):
                    colors[i].mix(Color([0.1, 0.6, 1], 3), self.rating)
                else:
                    colors[i].dim(1-self.rating)

            return {'output': colors, 'isEngaged': (rating>0)}
        
attributes = {
    'in': {'colors': 'Colors', 'threshold': 'Value', 'response': 'Value'},
    'out': {'output': 'Colors', 'isEngaged': 'Bool'},
    'class': pohotosensitivityBreak,
    'type': 'animation',
    'name': 'pohotosensitivity Break',
    'description': 'Node, that tries to block photosensitivity triggering light patterns.'
}
