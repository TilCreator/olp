from os.path import dirname, basename, isfile, realpath, join
import glob
import imp

__all__ = [basename(f)[:-3] for f in glob.glob(dirname(__file__)+"/*.py") if isfile(f) and not f.endswith('__init__.py')]

animations = {}
for name in __all__:
    animations[name] = imp.load_source('module.name', join(dirname(realpath(__file__)), f'{name}.py')).config
