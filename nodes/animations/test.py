import math

def animation(config, animation):
    # config = {'active': True, 'delay': 0.01, 'driver': 'wifiWS2', 'length': 150, 'ip': '151.217.161.139', 'port': 2342}
    # animation = {'name': 'test', 'state': 0.3729264497757008, 'speed': 0.5, 'brightness': 1.0, 'brightnessSpeed': 0, 'args': {'colors': [[1, 1, 1], [0, 0, 0]]}}
    leds = []
    for i in range(config['length']):
        if i - animation['state'] * config['length'] != 0:
            g = min(1 - math.log(abs(i - animation['state'] * config['length'])), 1)
        else:
            g = 0

        leds.append((0, g * animation['brightness'], animation['state'] * 0.5 * animation['brightness']))

    # leds = [(0, 0, 0), (1, 1, 1), ...]
    return leds


config = {
    'name': 'test',
    'function': animation,
    'args': {}
}
