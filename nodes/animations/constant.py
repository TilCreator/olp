def animation(config, animation):
    return [tuple([animation['args']['color'][i] * animation['brightness'] for i in range(3)]) for _ in range(config['length'])]


config = {
    'name': 'constant',
    'function': animation,
    'args': {'color': 'color'}
}
