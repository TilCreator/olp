def animation(config, animation):
    state = int(animation['state'] * len(animation['args']['colors']))

    return [tuple([animation['args']['colors'][state][i] * animation['brightness'] for i in range(3)]) for _ in range(config['length'])]


config = {
    'name': 'strobo',
    'function': animation,
    'args': {'colors': 'colors'}
}
