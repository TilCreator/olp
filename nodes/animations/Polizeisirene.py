import math

def gradient (minVal, maxVal, peak, current):
    if current > maxVal:
        return 0
    if current < minVal:
        return 0
    if current == peak:
        return 1
    if current <peak:
        return (math.cos((current-minVal)*math.pi)/(peak-minVal))*(-0.5)+0.5
        
    return (math.cos((current-maxVal)*math.pi)/(maxVal-peak))*(0.5)+0.5

def animation (config, animation):
    # config = {'active': True, 'delay': 0.01, 'driver': 'wifiWS2', 'length': 150, 'ip': '151.217.161.139', 'port': 2342}
    # animation = {'name': 'test', 'state': 0.3729264497757008, 'speed': 0.5, 'brightness': 1.0, 'brightnessSpeed': 0, 'args': {'colors': [[1, 1, 1], [0, 0, 0]]}}
    leds = []
    redMin = (animation['state']-0.2)*config['length']*0.4
    redMax = (animation['state']+0.1)*config['length']*0.4
    redPeak = (animation['state'])*config['length']*0.4
    
    blueMin = 1-(animation['state']-0.2)*config['length']*0.4
    blueMax = 1-(animation['state']+0.1)*config['length']*0.4
    bluePeak = 1-(animation['state'])*config['length']*0.4

    whiteMin = config['length']*0.45
    whiteMax = config['length']*0.45
    whitePeak = config['length']*0.45

    for i in range(config['length']):
        
        leds.append((gradient(redMin, redMax, redPeak, i), 0, gradient(blueMin, blueMax, bluePeak, i)))

    # leds = [(0, 0, 0), (1, 1, 1), ...]
    return leds


config = {
    'name': 'PolizeiSirene',
    'function': animation,
    'args': {}
}