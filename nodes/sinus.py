from math import pi, sin
from color import Color


def animation(config, animation, state=None, bg_color=Color([0, 0.2, 0.6]), color=Color([0, 1, 0]), scale=1):
    if state is None: state = animation['state']

    leds = []
    for i in range(config['length']):
        val = sin(pi * 2 * (i / config['length']) + pi * 2 * state - pi * 1.5)
        val = (val ** scale) * val
        if isinstance(val, complex):
            val = val.real
        val = max(val, 0)

        leds.append(Color([color.getRgb()[j] * val + bg_color.getRgb()[j] * (1 - val) for j in range(3)]))

    return {'colors': leds}


attributes = {
    'name': 'sinus',
    'description': 'Sinus animation',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Float', 'bg_color': 'Color', 'color': 'Color', 'scale': 'Float'},
    'out': {'colors': 'Colors'}
}
