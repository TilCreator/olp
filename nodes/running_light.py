from color import Color


def animation(config, animation, state=None, bg_color=Color([0, 0, 0]), color=Color([1, 1, 1])):
    if state is None: state = animation['state']

    return {'colors': [color if i == int(config['length'] * (1 - state)) else bg_color for i in range(config['length'])]}


attributes = {
    'name': 'Running light',
    'description': 'It runs and runs and ...',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Float', 'bg_color': 'Color', 'color': 'Color'},
    'out': {'colors': 'Colors'}
}
