from color import Color


def animation(config, animation, state=None, length=None, saturation=None, scale=None):
    if state is None: state = animation['state']
    if length is None: length = config['length']
    if saturation is None: saturation = 1
    if scale is None: scale = 2

    leds = [Color([state+(i/length*scale),saturation,1], 3) for i in range(length)]


    return {'leds': leds}


attributes = {
    'name': 'rainbow',
    'description': 'Rainbow animation',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Float', 'length': 'Number', 'saturation': 'Float', 'scale': "Float"},
    'out': {'leds': 'Colors'}
}
