from math import pi, sin
from color import Color


def animation(config, animation, state=None, color1=[0, 0.2, 0.6], color2=[0, 1, 0]):
    if state is None: state = animation['state']

    leds = []
    for i in range(config['length']):
        val = max(sin(2 * pi * state), 0)
        leds.append(Color([color1.getRgb()[j] * val + color2.getRgb()[j] * (1 - val) for j in range(3)]))

    return {'colors': leds}


attributes = {
    'name': 'blink_smooth',
    'description': 'Blink smooth animation',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Float', 'color1': 'Color', 'color2': 'Color'},
    'out': {'colors': 'Colors'}
}
