def add(config, animation, number1=0, number2=0):
    return {'result': number1 + number2}


def substract(config, animation, number1=0, number2=0):
    return {'result': number1 - number2}


def multiply(config, animation, number1=0, number2=0):
    return {'result': number1 * number2}


def divide(config, animation, number1=0, number2=0):
    return {'result': number1 * number2}


def cap(config, animation, number=0, min_number=0, max_number=1):
    return {'result': min(max(number, min_number), max_number)}


def mod(config, animation, number=0, divisor=1):
    return {'result': number % divisor}


attributes = [
    {
        'in': {'number1': 'Float', 'number2': 'Float'},
        'out': {'result': 'Float'},
        'function': add,
        'type': 'converter',
        'name': 'add',
        'description': 'Adds two numbers together'
    },
    {
        'in': {'number1': 'Float', 'number2': 'Float'},
        'out': {'result': 'Float'},
        'function': substract,
        'type': 'converter',
        'name': 'substract',
        'description': 'Substract one number from another'
    },
    {
        'in': {'number1': 'Float', 'number2': 'Float'},
        'out': {'result': 'Float'},
        'function': multiply,
        'type': 'converter',
        'name': 'multiply',
        'description': 'Multiply two numbers'
    },
    {
        'in': {'number1': 'Float', 'number2': 'Float'},
        'out': {'result': 'Float'},
        'function': divide,
        'type': 'converter',
        'name': 'divide',
        'description': 'Divide one number throu another'
    },
    {
        'in': {'number': 'Float', 'min_number': 'Float', 'max_number': 'Float'},
        'out': {'result': 'Float'},
        'function': cap,
        'type': 'converter',
        'name': 'cap',
        'description': 'Cap number between a min and a max value'
    },
    {
        'in': {'number': 'Float', 'divisor': 'Float'},
        'out': {'result': 'Float'},
        'function': mod,
        'type': 'converter',
        'name': 'Modulo',
        'description': 'Take the modulo of a number by the divisor'
    }
]
