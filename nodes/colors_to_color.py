from color import Color

def conv(config, animation, colors=[Color()], index=1):
    return {'color': colors[index]}


attributes = {
    'in': {'colors': 'Colors', 'index': 'Number'},
    'out': {'color': 'Color'},
    'function': conv,
    'type': 'converter',
    'name': 'Colors to Color',
    'description': 'Extracts one color from a color list'
}
