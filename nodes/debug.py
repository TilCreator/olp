def animation(config, animation, state=None, brightness=None):
    from color import color
    if state is None: state = animation['state']
    if brightness is None: brightness = animation['brightness']

    colors = []
    colors.append(color([state,1,1], 'hsv'))
    colors.append(color([state,1,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,1,0.8], 'hsv'))
    colors.append(color([state,1,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,1,0.6], 'hsv'))
    colors.append(color([state,1,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,1,0.4], 'hsv'))
    colors.append(color([state,1,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,1,0.2], 'hsv'))
    colors.append(color([state,1,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    

    colors.append(color([state,0.75,1], 'hsv'))
    colors.append(color([state,0.75,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.75,0.8], 'hsv'))
    colors.append(color([state,0.75,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.75,0.6], 'hsv'))
    colors.append(color([state,0.75,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.75,0.4], 'hsv'))
    colors.append(color([state,0.75,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.75,0.2], 'hsv'))
    colors.append(color([state,0.75,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    
    colors.append(color([state,0.5,1], 'hsv'))
    colors.append(color([state,0.5,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.8], 'hsv'))
    colors.append(color([state,0.5,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.6], 'hsv'))
    colors.append(color([state,0.5,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.4], 'hsv'))
    colors.append(color([state,0.5,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.2], 'hsv'))
    colors.append(color([state,0.5,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    
    colors.append(color([state,0.5,1], 'hsv'))
    colors.append(color([state,0.5,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.8], 'hsv'))
    colors.append(color([state,0.5,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.6], 'hsv'))
    colors.append(color([state,0.5,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.4], 'hsv'))
    colors.append(color([state,0.5,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.5,0.2], 'hsv'))
    colors.append(color([state,0.5,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    
    colors.append(color([state,0.25,1], 'hsv'))
    colors.append(color([state,0.25,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.25,0.8], 'hsv'))
    colors.append(color([state,0.25,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.25,0.6], 'hsv'))
    colors.append(color([state,0.25,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.25,0.4], 'hsv'))
    colors.append(color([state,0.25,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.25,0.2], 'hsv'))
    colors.append(color([state,0.25,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))

    
    colors.append(color([state,0.0,1], 'hsv'))
    colors.append(color([state,0.0,1], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.0,0.8], 'hsv'))
    colors.append(color([state,0.0,0.8], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.0,0.6], 'hsv'))
    colors.append(color([state,0.0,0.6], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.0,0.4], 'hsv'))
    colors.append(color([state,0.0,0.4], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    colors.append(color([state,0.0,0.2], 'hsv'))
    colors.append(color([state,0.0,0.2], 'hsv'))
    colors.append(color([0,0,0], 'hsv'))
    
    leds = []
    x = 0
    for i in range(len(colors)-1):
        if x == 0:
            leds.append(colors[i].getRgbEncoded() + [0])
        if x == 1:
            leds.append(colors[i].getRgbwEncoded())
        if x == 2: x = 0

    return {'leds': leds}


attributes = {
    'name': 'debug',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Number', 'brightness': 'Number'},
    'out': {'leds': 'Colors'}
}
