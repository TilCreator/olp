def bool_to_number(config, animation, bool=False):
    return {'number': int(bool)}


attributes = [
    {
        'name': 'bool_to_float',
        'description': 'Convert bool to float',
        'type': 'converter',
        'function': bool_to_number,
        'in': {'bool': 'Bool'},
        'out': {'number': 'Float'}
    },
    {
        'name': 'bool_to_number',
        'description': 'Convert bool to number',
        'type': 'converter',
        'function': bool_to_number,
        'in': {'bool': 'Bool'},
        'out': {'number': 'Number'}
    }
]
