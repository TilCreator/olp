def gradientArray(config, min = 0, max = None, include = True):
    if max == None: max = config['length']
    out = []
    for i in range(15):
        out.append(gradient(i, min, max, include))
    return out

def gradient (current = 0.42, min = 0, max = 1, include= False):
    if ((min<max and (current >= max or current <= min) and not include) or
        (min<max and (current > max or current < min) and include) or
        (min>max and (current <= max or current >= min) and not include) or
        (min>max and (current < max or current > min) and include)):
        return 0
    current = current - min
    max = max - min
    return current * 1/max

def test():
    print (gradient(-3, -3, 6, False), gradient(-1, -3, 6, False), gradient(0, -3, 6, False), gradient(1, -3, 6, False), gradient(2, -3, 6, False), gradient(6, -3, 6, False))
    print (gradient(2, 7, 3, True), gradient(7, 7, 3, True), gradient(5, 7, 3, True), gradient(3, 7, 3, False))
    print (gradientArray(8, 3))

if __name__ == '__main__':
    test()

attributes = [ {
    'name': 'GradientArray',
    'description': 'Makes a gradient interpolating between 0 and 1 between a min and max point.',
    'type': 'converter',
    'function': gradientArray,
    'in': {'min': 'Float', 'max': 'Float', 'include peaks': 'Boolean'},
    'out': {'gradient Array': 'Floats'}
    },{
    'name': 'Gradient',
    'description': 'Makes a gradient interpolating between 0 and 1 given a min and max Float.',
    'type': 'converter',
    'function': gradient,
    'in': {'min': 'Float', 'max': 'Float', 'include peaks': 'Boolean'},
    'out': {'gradient Value': 'Float'}
    }
]
