from os.path import dirname, basename, isfile, realpath, join
from collections import OrderedDict
from copy import deepcopy
import importlib.util
import glob
import sys

sys.path.append(join(dirname(dirname(realpath(__file__))), 'color.py'))

_data_types_specs = importlib.util.spec_from_file_location('module.name', join(dirname(realpath(__file__)), '_data_types.py'))
_data_types = importlib.util.module_from_spec(_data_types_specs)
_data_types_specs.loader.exec_module(_data_types)
data_types = _data_types.attributes

global_vars = {}

__all__ = [basename(f)[:-3] for f in glob.glob(dirname(__file__)+"/*.py") if isfile(f) and not f[f.rfind('/') + 1:].startswith('_')]

nodes = {}
for name in __all__:
    node_specs = importlib.util.spec_from_file_location('module.name', join(dirname(realpath(__file__)), f'{name}.py'))
    node = importlib.util.module_from_spec(node_specs)
    node_specs.loader.exec_module(node)
    config = node.attributes

    if type(config) is list:
        for i, sub_config in enumerate(config):
            nodes[f'{name}_{i}'] = sub_config
    else:
        nodes[name] = config

for data_type in data_types:
    nodes[data_type] = {
        'name': f'Static {data_type} input',
        'description': 'Static input',
        'in': {},
        'out': {data_type: data_type}
    }

nodes['out'] = {
    'name': 'out',
    'description': 'The final output, must only exists once.',
    'in': {'leds': 'Colors', 'state': 'Float', 'speed': 'Float', 'brightness': 'Float', 'brightnessSpeed': 'Float'},
    'out': {}
}

nodes['in'] = {
    'name': 'in',
    'description': 'Dynamic input for animation parameters',
    'in': {},
    'out': {'state': 'Float', 'delta_time': 'Float', 'speed': 'Float', 'brightness': 'Float', 'brightnessSpeed': 'Float'}
}


class nbpExeption(Exception):
    pass


def update_global_vars(vars=global_vars):
    global global_vars, nodes
    global_vars = vars

    nodes['globals'] = {
        'name': 'globals',
        'description': 'Dynamic input for outside',
        'in': {},
        'out': {name: val[0] for name, val in global_vars.items()}
    }


def get_available():
    out = {}

    out['nodes'] = {}
    for node in nodes:
        out['nodes'][node] = {}
        for key in  ['name', 'description', 'icon']:
            if key in nodes[node]:
                out['nodes'][node][key] = nodes[node][key]
        for direction in ['in', 'out']:
            out['nodes'][node][f'{direction}ports'] = []
            for name, type in nodes[node][direction].items():
                out['nodes'][node][f'{direction}ports'].append({'name': name, 'type': type})

    out['data_types'] = {}
    for data_type in data_types:
        out['data_types'][data_type] = {'html': data_types[data_type]['html'], 'default': data_types[data_type]['out_function'](data_types[data_type]['default_value'])}

    return out


class effect():
    def __init__(self, raw_effect, config):
        self.config = config
        self.leds = [*[(0, 0, 0)] * self.config['length']]

        out_nodes = [node for node in raw_effect['processes'] if raw_effect['processes'][node]['component'] == 'out']

        if len(out_nodes) > 1 or len(out_nodes) == 0:
            raise nbpExeption('Exactly one out node required')
        out_node = out_nodes[0]

        for id, node in raw_effect['processes'].items():
            for port in nodes[node['component']]['in']:
                connections = [connection for connection in raw_effect['connections'] if connection['tgt']['process'] == id and connection['tgt']['port'] == port]

                if len(connections) > 1:
                    raise nbpExeption(f'Max one input per port possible (id: {id})')

        count = 0
        todo_nodes = [out_node]
        node_list = {out_node: 0}
        while todo_nodes:
            new_todo_nodes = []
            for node in todo_nodes:
                connected_nodes = [connection['src']['process'] for connection in raw_effect['connections'] if connection['tgt']['process'] == node]
                connected_nodes = list(dict.fromkeys(connected_nodes))
                for connected_node in connected_nodes:
                    node_list[connected_node] = node_list[node] + 1

                    if connected_node not in new_todo_nodes:
                        new_todo_nodes.append(connected_node)

            todo_nodes = new_todo_nodes

            count += 1

            if count > len(raw_effect['processes']) ** 2 + 1:
                raise nbpExeption(f'Loop detected')

        node_list = sorted(node_list, key=node_list.get, reverse=True)

        self.prepeared_node_list = OrderedDict()
        for node in node_list:
            node_name = raw_effect['processes'][node]['component']
            node_type = nodes[node_name]

            n = {'id': node, 'node_type': node_type}
            n['return'] = {}
            for name, data_type in node_type['out'].items():
                n['return'][name] = data_types[data_type]['default_function'](self.config)

            if node_name == 'in':
                n['function'] = self.input
            elif node_name == 'out':
                n['function'] = self.output
            elif node_name == 'globals':
                n['function'] = self.globals
            elif node_name in data_types:
                n['function'] = lambda config, animation: None
                n['return'] = deepcopy({node_name: data_types[node_name]['in_function'](raw_effect['static_vars'][node])})
            else:
                if 'class' in node_type:
                    n['object'] = node_type['class'](config)
                    n['function'] = n['object'].run
                else:
                    n['function'] = node_type['function']

            self.prepeared_node_list[node] = n

        for id, node in self.prepeared_node_list.items():
            in_connections = [connection for connection in raw_effect['connections'] if connection['tgt']['process'] == id]

            node['args'] = {}
            for name in node['node_type']['in']:
                in_connection = [in_connection for in_connection in in_connections if name == in_connection['tgt']['port']]
                if len(in_connection) > 0:
                    in_connection = in_connection[0]
                    node['args'][name] = (in_connection['src']['process'], in_connection['src']['port'])

    def run(self, speed, brightnessSpeed, state, brightness, delta_time):
        self.speed = speed
        self.brightnessSpeed = brightnessSpeed
        self.state = state
        self.brightness = brightness
        self.delta_time = delta_time

        for id, node in self.prepeared_node_list.items():
            args = {}
            for name, path in node['args'].items():
                args[name] = self.prepeared_node_list[path[0]]['return'][path[1]]

            result = node['function'](self.config, {'speed': self.speed, 'brightnessSpeed': self.brightnessSpeed, 'state': self.state, 'brightness': brightness, 'delta_time': self.delta_time}, **args)
            if result is not None:
                node['return'] = result

        return {'leds': self.leds, 'speed': self.speed, 'state': self.state, 'brightnessSpeed': self.brightnessSpeed, 'brightness': self.brightness}

    def input(self, config, animation):
        return {'speed': self.speed, 'delta_time': self.delta_time, 'state': self.state, 'brightnessSpeed': self.brightnessSpeed, 'brightness': self.brightness}

    def globals(self, config, animation):
        return {name: deepcopy(val[1]) for name, val in global_vars.items()}

    def output(self, config, animation, leds=None, speed=None, state=None, brightnessSpeed=None, brightness=None):
        if leds is not None: self.leds = leds
        if speed is not None: self.speed = speed
        if brightnessSpeed is not None: self.brightnessSpeed = brightnessSpeed
        if state is not None: self.state = state
        if brightness is not None: self.brightness = brightness


update_global_vars()
