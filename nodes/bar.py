from color import Color

def bar(config, animation, colors=[Color()], state=None):
    if state is None: state = animation['state']

    bar_n = state * len(colors)
    bar_a = [1 for _ in range(int(bar_n))]
    bar_a += [bar_n - int(bar_n)]
    bar_a += [0 for _ in range(len(colors) - (int(bar_n) + 1))]

    colors = [Color([color.getRgb()[i] * bar_a[j] for i in range(3)]) for j, color in enumerate(colors)]

    return {'colors': colors}


attributes = {
    'in': {'colors': 'Colors', 'state': 'Float'},
    'out': {'colors': 'Colors'},
    'function': bar,
    'type': 'animation',
    'name': 'Bar',
    'description': 'Displays a Dynamic bar'
}
