from color import Color


def animation(config, animation, state=None, color1=[0, 0.2, 0.6], color2=[0, 1, 0]):
    if state is None: state = animation['state']

    leds = []
    for i in range(config['length']):
        val = int(state < 0.5)
        leds.append(Color([color1.getRgb()[j] * val * brightness + color2.getRgb()[j] * (1 - val) * brightness for j in range(3)]))

    return {'colors': leds}


attributes = {
    'name': 'blink_sharp',
    'description': 'Blink sharp animation',
    'type': 'animation',
    'function': animation,
    'in': {'state': 'Float', 'color1': 'Color', 'color2': 'Color'},
    'out': {'colors': 'Colors'}
}
