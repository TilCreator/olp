from color import Color

def truncate(config, animation, colors=[Color()], length=None):
    if length is None: length = config['length']

    return {'colors': colors[:length]}


attributes = {
    'in': {'colors': 'Colors', 'length': 'Number'},
    'out': {'colors': 'Colors'},
    'function': truncate,
    'type': 'converter',
    'name': 'Truncate Colors',
    'description': 'Truncaes a list of colors'
}
