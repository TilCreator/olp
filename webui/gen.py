import os
import json
import shutil
import jinja2
import subprocess
import hamlish_jinja


cp_files = ['less.js', 'index.less',
            'haml.js',
            'the-graph.js', 'the-graph_vendor.js', 'the-graph-dark.css', 'fonts',
            'noty.min.js', 'noty.css', 'noty_sunset.css',
            'sweetalert2.min.js', 'sweetalert2.css']
haml_files = ['index.haml']
coffee_files = ['index.coffee']


os.chdir(os.path.dirname(os.path.realpath(__file__)))

try:
    with open(os.path.join('..', 'config.json')) as f:
        config = json.load(f)
except FileNotFoundError:
    print('Config not found, please create config.json (use config.example.json as template)')
    exit()


print('Cleaning up build')
if os.path.exists('build'):
    shutil.rmtree('build')
os.mkdir('build')


env = jinja2.Environment(extensions=[hamlish_jinja.HamlishExtension])
if config['log_level'] == 'DEBUG':
    env.hamlish_mode = 'debug'

for file in haml_files:
    print(f'Compiling "{file}"')
    with open(file, 'r') as f_in:
        with open(os.path.join('build', file[:file.find('.')] + '.html'), 'w') as f_out:
            f_out.write(env.hamlish_from_string(f_in.read()).render(config=config))


coffee_options = []
if config['log_level'] == 'DEBUG':
    coffee_options.append('-m')

for file in coffee_files:
    print(f'Compiling "{file}"')
    subprocess.call(['coffee', '-c', *coffee_options, '-o', os.path.join('build', file[:file.find('.')] + '.js'), file])


for file in cp_files:
    print(f'Copying "{file}"')
    if os.path.isfile(file):
        shutil.copyfile(file, os.path.join('build', file))
    else:
        shutil.copytree(file, os.path.join('build', file))
