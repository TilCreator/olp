render_template = (template, data=this) ->
    Haml.render(template, {'context': data})

find_keyAndVal_in_array = (arr, key, val, casesensitive=false) ->
    for i, o of arr
        if casesensitive
            if key of o and o[key] == val
                return o
        else
            if key of o and o[key].toLowerCase() == val.toLowerCase()
                return o
    return null

@encode = (str) ->
    encodeURI(str).replace(/%/g, 'specialchar');

change_tab = (tab) ->
    @active_tab = tab

    document.querySelectorAll('#tabs div.tab').forEach((e) ->
        e.classList.remove('active')
    )
    if document.querySelector("#tabs div.tab##{tab}_tab")
        document.querySelector("#tabs div.tab##{tab}_tab").classList.add('active')

init_dashboard = () ->
    change_tab('dashboard')

    document.querySelector('#content').innerHTML = render_template("""
%div.container
    %h1 < Lights
    :each name, device in devices
        %div.subcontainer{id: "device_" + window.encode(name)}
            %h1= name

            %div
                %input.switch{id: "device_switch_" + window.encode(name), name: name, type: "checkbox"}
                %label{for: "device_switch_" + name}
                    On/Off

            %div
                %label{for: "device_brightness_" + name}
                    Brightness
                %input{id: "device_brightness_" + window.encode(name), name: name, type: "range", min: 0, max: 1, step: 0.01}

            %div
                %label{for: "device_speed_" + name}
                    Speed
                %input{id: "device_speed_" + window.encode(name), name: name, type: "range", min: -10, max: 10, step: 0.01}
%div.container
    %h1 < Globals
    :each name, global_var in global_vars
        %div.subcontainer{id: "global_" + window.encode(name)}
            %h1= name

            %div.value= data_types[global_var[0]]
""")

    setTimeout(() ->
        for name, device of @devices
            document.querySelector("#device_switch_#{encode(name)}").checked = device.animation.active
            document.querySelector("#device_switch_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.active = this.checked
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_brightness_#{encode(name)}").value = device.animation.brightness
            document.querySelector("#device_brightness_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.brightness = parseFloat(this.value)
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_speed_#{encode(name)}").value = device.animation.speed
            document.querySelector("#device_speed_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.speed = parseFloat(this.value)
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

        for name, global_var of @global_vars
            document.querySelector("#global_#{encode(name)} .value .out").name = name
            document.querySelector("#global_#{encode(name)} .value .out").value = global_var[1]
            () ->
                eval(this.attributes.oninit.nodeValue)
            .call(document.querySelector("#global_#{encode(name)} .value .out"))

            document.querySelector("#global_#{encode(name)} .value .out").onchange = (e) ->
                window.global_vars[this.name][1] = this.value
                ws_send('set_global', {'name': this.name, 'data': window.global_vars[this.name][1]})
    , 1)

update_dashboard = () ->
    for name, device of @devices
        document.querySelector("#device_switch_#{encode(name)}").checked = device.animation.active

        document.querySelector("#device_brightness_#{encode(name)}").value = device.animation.brightness

        document.querySelector("#device_speed_#{encode(name)}").value = device.animation.speed

    for name, global_var of @global_vars
        document.querySelector("#global_#{encode(name)} .value .out").value = global_var[1]
        () ->
            eval(this.attributes.oninit.nodeValue)
        .call(document.querySelector("#global_#{encode(name)} .value .out"))

init_lights = () ->
    change_tab('lights')

    document.querySelector('#content').innerHTML = render_template("""
:each name, device in devices
    %div.container{id: "device_" + window.encode(name)}
        %h1= name

        %button{id: "device_remove_" + window.encode(name)}
            remove

        %div.subcontainer
            %h1 < Config

            %div
                %input.switch{id: "device_switch_" + window.encode(name), name: name, type: "checkbox"}
                %label{for: "device_switch_" + name}
                    On/Off

            %div
                %label{for: "device_brightness_" + name}
                    Brightness
                %input{id: "device_brightness_" + window.encode(name), name: name, type: "range", min: 0, max: 1, step: 0.01}

            %div
                %label{for: "device_speed_" + name}
                    Speed
                %input{id: "device_speed_" + window.encode(name), name: name, type: "range", min: -10, max: 10, step: 0.01}

        %div.subcontainer
            %h1 < Driver

            %select{id: "device_driver_" + window.encode(name), name: name}
                :each d_name in Object.keys(drivers).sort()
                    %option{name: d_name}= d_name

            :if device['config']['driver'] !== null
                :each d_name, type_default in drivers[device.config.driver].args
                    %div.subcontainer{id: "config_" + window.encode(name) + "_" + window.encode(d_name)}
                        %h1= d_name

                        %div.value= data_types[type_default[0]]

        %div.subcontainer
            %h1 < Effect

            %select{id: "device_effect_" + window.encode(name), name: name}
                %option{name}
                :each e_name in Object.keys(effects).sort()
                    %option{name: e_name}= e_name

%div.container
    %h1 < Add

    %input{id: "device_add_name", type: "text", placeholder: "name"}

    %select{id: "device_add_driver"}
        :each d_name in Object.keys(drivers).sort()
            %option{name: d_name}= d_name

    %button{id: "devices_add"}
        add
""")

    setTimeout(() ->
        document.querySelector("#devices_add").onclick = (e) ->
            name = if document.querySelector("#device_add_name").value != '' then document.querySelector("#device_add_name").value else "light_#{Math.random().toString(36).substring(7)}"
            ws_send('add_device', {'name': name, 'data': {'config': {'driver': Object.keys(window.drivers).sort()[document.querySelector("#device_add_driver").selectedIndex]}}})

        for name, device of @devices
            document.querySelector("#device_remove_#{encode(name)}").name = name
            document.querySelector("#device_remove_#{encode(name)}").onclick = (e) ->
                ws_send('remove_device', {'name': this.name})

            document.querySelector("#device_switch_#{encode(name)}").checked = device.animation.active
            document.querySelector("#device_switch_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.active = this.checked
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_brightness_#{encode(name)}").value = device.animation.brightness
            document.querySelector("#device_brightness_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.brightness = parseFloat(this.value)
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_speed_#{encode(name)}").value = device.animation.speed
            document.querySelector("#device_speed_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.speed = parseFloat(this.value)
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_driver_#{encode(name)}").selectedIndex = Object.keys(@drivers).sort().indexOf(device.config.driver)
            document.querySelector("#device_driver_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].config.driver = Object.keys(window.drivers).sort()[this.selectedIndex]

                for key, value in window.drivers[window.devices[this.name].config.driver]
                    if not window.devices[this.name].config[key]?
                        window.devices[this.name].config[key] = value[1]

                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            document.querySelector("#device_effect_#{encode(name)}").selectedIndex = Object.keys(@effects).sort().indexOf(device.animation.name) + 1
            document.querySelector("#device_effect_#{encode(name)}").onchange = (e) ->
                window.devices[this.name].animation.name = Object.keys(window.effects).sort()[this.selectedIndex - 1]
                if not window.devices[this.name].animation.name?
                    window.devices[this.name].animation.name = null
                ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})

            for d_name, type_default of @drivers[device.config.driver].args
                document.querySelector("#config_#{encode(name)}_#{encode(d_name)} .value .out").name = name
                document.querySelector("#config_#{encode(name)}_#{encode(d_name)} .value .out").d_name = d_name
                document.querySelector("#config_#{encode(name)}_#{encode(d_name)} .value .out").value = if device.config[d_name]? then device.config[d_name] else type_default[1]
                () ->
                    eval(this.attributes.oninit.nodeValue)
                .call(document.querySelector("#config_#{encode(name)}_#{encode(d_name)} .value .out"))

                document.querySelector("#config_#{encode(name)}_#{encode(d_name)} .value .out").onchange = (e) ->
                    window.devices[this.name].config[this.d_name] = this.value
                    ws_send('set_device', {'name': this.name, 'data': window.devices[this.name]})
    , 1)

init_globals = () ->
    change_tab('globals')

    document.querySelector('#content').innerHTML = render_template("""
%div.container
    %h1 < Globals
    :each name, global_var in global_vars
        %div.subcontainer{id: "global_" + window.encode(name)}
            %h1= name

            %button{id: "global_remove_" + window.encode(name)}
                remove

            %div.value= data_types[global_var[0]]
%div.container
    %h1 < Add

    %input{id: "global_add_name", type: "text", placeholder: "name"}

    %select{id: "global_add_type"}
        :each d_name in Object.keys(data_types).sort()
            %option{name: d_name}= d_name

    %button{id: "global_add"}
        add
""")

    setTimeout(() ->
        document.querySelector("#global_add").onclick = (e) ->
            name = if document.querySelector("#global_add_name").value != '' then document.querySelector("#global_add_name").value else "global_#{Math.random().toString(36).substring(7)}"
            ws_send('add_global', {'name': name, 'type': Object.keys(window.data_types).sort()[document.querySelector("#global_add_type").selectedIndex]})

        for name, global_var of @global_vars
            document.querySelector("#global_#{encode(name)} .value .out").name = name
            document.querySelector("#global_#{encode(name)} .value .out").value = global_var[1]
            () ->
                eval(this.attributes.oninit.nodeValue)
            .call(document.querySelector("#global_#{encode(name)} .value .out"))

            document.querySelector("#global_#{encode(name)} .value .out").onchange = (e) ->
                window.global_vars[this.name][1] = this.value
                ws_send('set_global', {'name': this.name, 'data': window.global_vars[this.name][1]})

            document.querySelector("#global_remove_#{encode(name)}").name = name
            document.querySelector("#global_remove_#{encode(name)}").onclick = (e) ->
                ws_send('remove_global', {'name': this.name})
    , 1)

init_effects = () ->
    change_tab('effects')

    document.querySelector('#content').innerHTML = render_template("""
%div.container
    %h1 < Effects
    :each name, effect in effects
        %div.subcontainer{id: "effect_" + window.encode(name)}
            %h1= name

            %button{id: "effect_remove_" + window.encode(name)}
                remove

            %button{id: "effect_edit_" + window.encode(name)}
                edit
%div.container
    %h1 < Add

    %input{id: "effect_add_name", type: "text", placeholder: "name"}

    %button{id: "effect_add"}
        add
""")

    setTimeout(() ->
        document.querySelector("#effect_add").onclick = (e) ->
            name = if document.querySelector("#effect_add_name").value != '' then document.querySelector("#effect_add_name").value else "effect_#{Math.random().toString(36).substring(7)}"
            ws_send('add_effect', {'name': name, 'data': {}})

        for name, effect of @effects
            document.querySelector("#effect_edit_#{encode(name)}").name = name
            document.querySelector("#effect_edit_#{encode(name)}").onclick = (e) ->
                edit_effect(this.name)

            document.querySelector("#effect_remove_#{encode(name)}").name = name
            document.querySelector("#effect_remove_#{encode(name)}").onclick = (e) ->
                ws_send('remove_effect', {'name': this.name})
    , 1)

edit_effect = (name) ->
    ws_send('get_nodes')
    ws_send('get_effect', {'name': name})

init_node_editor = (name, data) ->
    change_tab('node_editor')
    @node_editor_active = true
    @node_editor_data = data

    document.querySelector('#content').innerHTML = render_template("""
%div#editor
%div#nav
%div#buttons
    %button#test_nodes
        test
    %button#save_nodes
        save
    %button#close_nodes
        close
    %div
        %select#nodes_selection
            :each name in Object.keys(nodes.nodes).sort()
                %option{name: name}= name

        %button#node_add
            Add
""")

    setTimeout(() ->
        document.querySelector("#close_nodes").onclick = (e) ->
            window.removeEventListener("resize", renderApp)

            @node_editor_active = false

            init_effects()

        document.querySelector("#test_nodes").onclick = (e) ->
            ws_send('test_effect', {'data': Object.assign(window.node_editor_data, window.appState.graph.toJSON())})

        document.querySelector("#save_nodes").onclick = (e) ->
            ws_send('set_effect', {'name': window.appState.name,'data': Object.assign(window.node_editor_data, window.appState.graph.toJSON())})

        document.querySelector("#node_add").onclick = (e) ->
            key = Object.keys(window.nodes.nodes).sort()[document.querySelector("#nodes_selection").selectedIndex]
            node_id = Math.round(Math.random()*100000).toString(36)
            node = window.nodes.nodes[key]

            if node.description == 'Static input'
                window.node_editor_data.static_vars[node_id] = window.nodes.data_types[key].default

            metadata = {
                label: node['name'],
                x: 50,
                y: 50
            }
            window.appState.graph.addNode(node_id, key, metadata);

        fbpGraph = window.TheGraph.fbpGraph

        deleteNode = (graph, itemKey, item) ->
            graph.removeNode(itemKey)

            if window.nodes.nodes[item.component].description == 'Static input'
                delete window.node_editor_data.static_vars[itemKey]
        deleteEdge = (graph, itemKey, item) ->
            graph.removeEdge(item.from.node, item.from.port, item.to.node, item.to.port)
        edit_static = (graph, itemKey, item) ->
            if window.nodes.nodes[item.component].description == 'Static input'
                Swal.fire({
                    title: window.nodes.nodes[item.component].name,
                    html: "<div id='node_editor_static_popup' class='value'>#{window.nodes.data_types[item.component].html}</div>",
                    showCloseButton: true,
                    confirmButtonText: 'Save',
                    onOpen: () ->
                        document.querySelector("#node_editor_static_popup.value .out").name = window.nodes.nodes[item.component].name
                        document.querySelector("#node_editor_static_popup.value .out").value = window.node_editor_data.static_vars[itemKey]
                        () ->
                            eval(this.attributes.oninit.nodeValue)
                        .call(document.querySelector("#node_editor_static_popup.value .out"))

                        document.querySelector("#node_editor_static_popup.value .out").onchange = (e) ->
                            e
                }).then((result) ->
                    window.node_editor_data.static_vars[itemKey] = document.querySelector('#node_editor_static_popup.value .out').value
                )

        contextMenus = {
            main: null,
            selection: null,
            nodeInport: null,
            nodeOutport: null,
            graphInport: null,
            graphOutport: null,
            edge: {
                icon: "long-arrow-right",
                s4: {
                    icon: "trash-o",
                    iconLabel: "delete",
                    action: deleteEdge
                }
            },
            node: {
                n4: {
                    icon: "fa-pencil",
                    iconLabel: "edit",
                    action: edit_static
                },
                s4: {
                    icon: "trash-o",
                    iconLabel: "delete",
                    action: deleteNode
                },
            },
            group: null
        }

        window.appState = {
            name: name,
            graph: new fbpGraph.Graph(),
            library: window.nodes.nodes,
            iconOverrides: {},
            theme: 'dark',
        }

        fitGraphInView = () ->
            editor.triggerFit()
        panEditorTo = () ->
            null
        editorPanChanged = (x, y, scale) ->
            window.appState.editorViewX = -x
            window.appState.editorViewY = -y
            window.appState.editorScale = scale
            renderNav()
        renderNav = () ->
            view = [
                window.appState.editorViewX, window.appState.editorViewY,
                window.innerWidth, window.innerHeight,
            ]
            props = {
                height: 162,
                width: 216,
                graph: window.appState.graph,
                onTap: fitGraphInView,
                onPanTo: panEditorTo,
                viewrectangle: view,
                viewscale: window.appState.editorScale,
            }

            element = React.createElement(TheGraph.nav.Component, props)
            ReactDOM.render(element, document.getElementById('nav'))

        renderApp = () ->
            editor = document.getElementById('editor')
            editor.className = 'the-graph-' + window.appState.theme

            props = {
                width: window.innerWidth,
                height: window.innerHeight,
                graph: window.appState.graph,
                library: window.appState.library,
                menus: contextMenus,
                nodeIcons: window.appState.iconOverrides,
                onPanScale: editorPanChanged,
            }

            for i, edge of props.graph.edges
                from_type = find_keyAndVal_in_array(props.library[props.graph.getNode(edge.from.node).component].outports, 'name', edge.from.port).type
                to_type = find_keyAndVal_in_array(props.library[props.graph.getNode(edge.to.node).component].inports, 'name', edge.to.port).type

                if from_type != to_type
                    props.graph.removeEdge(edge.from.node, edge.from.port, edge.to.node, edge.to.port)

                    new Noty({
                            'type': 'warning',
                            'text': 'Illegal connection attempt',
                            'closeWith': ['click', 'button']
                        }).show()

            console.log('render', props)

            editor = document.getElementById('editor')
            editor.width = props.width
            editor.height = props.height
            element = React.createElement(TheGraph.App, props)
            ReactDOM.render(element, editor)

            renderNav()
        renderApp()

        window.addEventListener("resize", renderApp)

        window.loadGraph = (data) ->
            fbpGraph.graph.loadJSON(JSON.stringify(data), (err, graph) ->
                if (err)
                    console.error(err)
                    return

                window.appState.graph = graph
                window.appState.graph.on('endTransaction', renderApp)
                renderApp()

                console.log('loaded', graph)
            )

        loadGraph(data)
    , 10)

show_effect_test = (data) ->
    if data.pass
        Swal.fire(
            'Effect Testing',
            'Effect was tested successfully without error',
            'success'
        )
    else
        marking = ''
        if data['nodes_error']['node_id'] != null
            marking = '<br>Node was marked'
        Swal.fire(
            'Effect Testing',
            "*Effect throws error at you*<br><br><pre>#{data['nodes_error']['long_error']}</pre>" + marking,
            'error'
        )

devices_update = () ->
    if @active_tab == 'dashboard'
        init_dashboard()
    else if @active_tab == 'lights'
        init_lights()

device_update = () ->
    if @active_tab == 'dashboard'
        update_dashboard()
    else if @active_tab == 'lights'
        init_lights()

effects_update = () ->
    if @active_tab == 'lights'
        init_lights()
    else if @active_tab == 'effects'
        init_effects()

globals_update = () ->
    if @active_tab == 'dashboard'
        init_dashboard()
    else if @active_tab == 'globals'
        init_globals()

ws_send = (cmd, data={}) ->
    console.log('Send:', cmd, data)
    @ws.send(JSON.stringify({'cmd': cmd, ...data}))

connect = () ->
    @ws = new WebSocket("ws://#{location.hostname}:#{config.ws_port}")

    @ws.onopen = () ->
        new Noty({
                'type': 'success',
                'text': 'Websocket connected',
                'timeout': 500,
                'queue': 'websocket',
                'killer': 'websocket',
                'closeWith': []
            }).show()

        ws_send('get_devices')
        ws_send('get_effects')
        ws_send('get_globals')
        ws_send('get_data_types')
        ws_send('get_nodes')
        ws_send('get_drivers')

        starting_interval = setInterval(() ->
            if @devices? and @global_vars? and @data_types?
                clearInterval(starting_interval)

                if not @node_editor_active
                    init_dashboard()

                document.querySelector('#dashboard_tab').onclick = init_dashboard
                document.querySelector('#lights_tab').onclick = init_lights
                document.querySelector('#globals_tab').onclick = init_globals
                document.querySelector('#effects_tab').onclick = init_effects
                # document.querySelector('#server_tab').onclick = init_server

                document.querySelector('#lock').classList.remove('active')
        , 200)

    @ws.onmessage = (evt) ->
        data = JSON.parse(evt.data)

        console.log("Got data:", data)

        if 'error' of data
            console.error('Websocket error:', data.error)
            new Noty({
                    'type': 'error',
                    'text': "Websocket error: #{data.error}"
                }).show()
        else if 'cmd' of data
            if ['get_devices', 'remove_device', 'add_device', 'change_device_name'].includes(data.cmd)
                window.devices = data.devices

                devices_update()
            else if ['get_device', 'set_device'].includes(data.cmd)
                window.devices[data.name] = data.data

                device_update()
            else if ['get_effects', 'remove_effect', 'add_effect', 'change_effect_name'].includes(data.cmd)
                window.effects = data.effects

                effects_update()
            else if ['get_effect'].includes(data.cmd)
                init_node_editor(data.name, data.data)
            else if ['test_effect'].includes(data.cmd)
                show_effect_test(data)
            else if ['get_globals', 'set_global', 'remove_global', 'add_global', 'change_global_name'].includes(data.cmd)
                window.global_vars = data.globals

                globals_update()
            else if data.cmd == 'get_data_types'
                window.data_types = data.data_types
            else if data.cmd == 'get_nodes'
                window.nodes = data.nodes
            else if data.cmd == 'get_drivers'
                window.drivers = data.drivers
            else if data.cmd == 'error_notification'
                new Noty({
                        'type': 'error',
                        'text': data.text,
                        'closeWith': ['button', 'click']
                    }).show()
            else if data.cmd == 'info_notification'
                new Noty({
                        'type': 'info',
                        'text': data.text,
                        'closeWith': ['button', 'click']
                    }).show()

    @ws.onclose = () ->
        new Noty({
                'type': 'error',
                'text': "Websocket closed",
                'queue': 'websocket',
                'killer': 'websocket',
                'closeWith': ['foo']
            }).show()

        document.querySelector('#lock').classList.add('active')

        setTimeout(connect, 500)

    @ws.onerror = (e) ->
        console.error('Websocket error', e)

        @ws.close()

@active_tab = null
@node_editor_active = false

window.onload = () ->
    connect()

Noty.overrideDefaults({
    layout: 'topRight',
    theme: 'sunset',
    closeWith: ['click', 'button']
});
